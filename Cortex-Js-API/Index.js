// ***************************************************************
// Copyright © 2018 Elastic Path Software Inc. All rights reserved.
// ***************************************************************

// File exposes all files in CortexJSLibs.
module.exports.Accounts = require('./ParserModules/Accounts.js');
module.exports.Checkout = require('./ParserModules/Checkout.js');
module.exports.Cart = require('./ParserModules/Cart.js');
module.exports.Items = require('./ParserModules/Items.js');
module.exports.Orders = require('./ParserModules/Orders.js');
module.exports.RecommendedItems = require('./ParserModules/RecommendedItems.js');
module.exports.Cortex = require('./Cortex.js');
module.exports.Utilities = require('./Utilities.js');

// Export Cognito Services if they exist.
try {
    const Cognito = require('cortex-cognito').Cognito;
    const CognitoCortex = require('cortex-cognito').CognitoCortex;
    module.exports.Cognito = Cognito;
    module.exports.CognitoCortex = CognitoCortex;
} catch (err) {
    console.log('Note: Did not export Cognito services');
}
