
# Overview

## API File Structure:

* Cortex.js - Holds all REST calls made to Cortex.

* Utilities.js - Holds generic cortex functions.

* ParserModules/Accounts.js - Holds functions that interface Cortex profiles resource.

* ParserModules/Cart.js - Holds functions that interface Cortex carts resource

* ParserModuiles/Checkout.js - Holds functions that interface the Cortex checkout process.

* ParserModules/Item.js - Holds all functions that deal with Cortex Items

* ParserModules/Orders.js - Holds all functions that interface with the Cortex Orders resource

* ParserModules/RecommendationItems.js - Holds all functions that deal with item relationships i.e crosssell, upsell, related items

* Config/Settings.js - config file that holds settings for the JSlibs

# Getting Started

The libraries are available as a node module, downloadable via

```

npm install cortex-js-api

```

You will be prompted with:  

```

Would you like to add dependancies necessary for Cognito Integration? input y or n>

```

If you intend to use the libraries with AWS cognito services and Cortex in Trusted Header Mode then input

```
y
```

else input:

```
n
```

Once the node module is installed you will need to import the corresponding modules based on functionality.  For completeness here is the code that will import all modules:

```

const CortexAPI = require('cortex-js-api');
const Cortex = CortexAPI.Cortex;
const Utilities = CortexAPI.Utilities;
const Cart = CortexAPI.Cart;
const Items = CortexAPI.Items;
const Checkout = CortexAPI.Checkout;
const Orders = CortexAPI.Orders;
const Accounts = CortexAPI.Accounts;

```

Before accessing cortex resources you are going to need to instantiate a Cortex object from the API.

```

$.when(Utilities.createCortexInstance(
  // REPLACE PARAMETERS TO YOUR OWN INSTANCE
  'fake.account@ep.com',
  'password',
  'http://35.163.108.181:8080/cortex',  
  'JWinery')
.done(
  (cortexInstance) => {
    console.log(cortexInstance);
  }
);

```

The 'cortexInstance' variable will be used as a parameter in all ParserModule functions to access cortex resources.  The next section will outline the usage of each function in the SDK.

NOTE:  Each one of the functions below uses jquery to handle promises.  However, if you are running the SDK in node you can interact with the library using natively available promise handling functionality.  For example the above function could be used like this:

```
Utilities.createCortexInstance(
  // REPLACE PARAMETERS TO YOUR OWN INSTANCE
  'fake.account@ep.com',
  'password',
  'http://35.163.108.181:8080/cortex',  
  'JWinery')
.then(
  (cortexInstance) => {
    console.log(cortexInstance);
  }
).catch((error) => {
    console.log('There has been some sort of error creating the cortex instance');
    console.log(error);
});;
```

# List of Available Cortex functions

## Utilities.js

### createCortexInstance

/**
 * [createCortexInstance - Used to return an authenticated cortex instance.]
 * @param  {[String]} username [the username of EPSHOPPER]
 * @param  {[String]} password [the password of EPSHOPPER]
 * @param  {[String]} baseUrl  [the baseUrl of your Cortex instance ex. http://35.163.108.181:8080/cortex]
 * @param  {[String]} scope    [the scope of EPSHOPPER]
 * @return {[Cortex]}          [authenticated cortex instance]
 */

#### Example Usage

```

$.when(Utilities.createCortexInstance('james.cameron@ep.com', 'password', 'http://35.163.108.181:8080/cortex', 'JWinery')).done(
  (authenticatedCortexInstance) => {
    console.log(authenticatedCortexInstance);
  }
);

```

### createAnonCortexInstance
/**
 * [createAnonCortexInstance - creates an anonymous login cortexInstance.]
 * @param  {[String]} baseUrl [the instance of Cortex]
 * @param  {[String]} scope   [the desired scope]
 * @return {[Cortex]}         [an Anonymouse Cortex Object]
 */

#### Example Usage

```

$.when(Utilities.createAnonCortexInstance('http://35.163.108.181:8080/cortex', 'JWinery')).done(
  (anonCortexInstance) => {
    console.log(anonCortexInstance);
  }
);

```

### createCognitoCortexInstance
/**
 * [createCognitoCortexInstance Function will create a Cognito cortex instance that is equipped to make calls to Cortex in Trusted header mode]
 * @param  {[type]} username        [The username of the user to be placed in header]
 * @param  {[type]} password        [The password of the user to be placed in header]
 * @param  {[type]} cognitoInstance [The Cognito Instance instantiated, containing configuration data for your current set up]
 * @return {[CognitoCortex]}                 [the CognitoCortex object]
 */

#### Example Usage

```

  Cognito = require('cortex-cognito').Cognito;  // NOTE: cortex-cognito only available if user inputted 'y' when prompted to    download cortex functionality

  cognitoInstance = new Cognito();

  $.when(
  Utilities.createCognitoCortexInstance('james.bond@ep.com', 'password, cognitoInstance)).done(
    (cognitoCortexInstance) => {
      console.log(cognitoCortexInstance);
    }
  );

```


## ParserModules/Accounts.js

### getAllAccountInformation

/**

* gets all account information from cortex (At the moment only address information - WIP).

* @param  {Cortex} cortexInstance - the Cortex Instance

* @return {Object} - Returns all account information in JSON structure

*/

#### Example Usage

```
$.when(Accounts.getAllAccountInformation(cortexInstance))
  .done((accountInformation) => {
    console.log(accountInformation)
  });
```


### registerUser

/**

* registers user

* @param  {Object} payload - All the information needed to register a user

* @param  {String} baseUrl - cortex base url

* @param  {String} scope   - store scope

* @return {Object} null

*/

#### Example Usage

```

// this payload will just register the user
const payload0 = {
  'firstName':'George',
  'lastName':'Harrison',
  'password':'password',
  'email':'george111.harrison@ep.com'
};

// this payload will register user with address and payment token
const payload1 = {
  firstName:'George',
  lastName:'Harrison',
  password:'password',
  email:'george111.harrison@ep.com',
  address: {
    countryName: 'US',
    extendedAddress: '',
    locality: 'Richmond',
    postalCode: 'v7d923',
    region: 'VA',
    streetAddress: '8392 Fake Street',
    lastName: 'Harrison',
    firstName: 'George'
  },
  payment: {
    displayName: 'VISA',
    paymentToken: '1234dfsf'
  }
};

$.when(Accounts.registerUser(payload1, 'http://35.163.108.181:8080/cortex', 'JWinery'))
  .done((cortexInstance) => {
    console.log(cortexInstance);
  })
  .fail((err) => {
    console.log(err);
  });

```


### addUserAttributesToAccount

/**
 * [addUserAttributesToAccount Adds an address and payment information to cortex account]

 * @param {Cortex} cortexInstance - the Cortex Instance

 * @param {JSON} payload        - The json structure that will carry address and payment information

 */

#### Example Usage

```

const payload = {
  address: {
    countryName: 'US',
    extendedAddress: '',
    locality: 'Richmond',
    postalCode: 'v7d923',
    region: 'VA',
    streetAddress: '8392 Fake Street',
    lastName: 'Bond',
    firstName: 'James'
  },
  payment: {
    displayName: 'VISA',
    paymentToken: '1234dfsf'
  }
};

$.when(Accounts.addUserAttributesToAccount(cortexInstance, payload))
  .done(() => {

  });
});

```

### getPaymentToken

/**
 * Returns the default payment token from a users profile.
 * @param {Cortex} cortexInstance - the Cortex Instance
 * @return {JQuery.promise}
 */

#### Example Usage

```
$.when(Accounts.getPaymentToken(cortexInstance)).done((tokenData) => {
  console.log(tokenData);
});
```
### getDefaultShippingAddress

/**
 * Returns the current user's default shipping address
 * @param  {[Cortex]} cortexInstance [The Cortex Instance in use]
 * @return {JQuery.promise}
 */

#### Example Usage

```
$.when(Accounts.getDefaultShippingAddress(cortexInstance)).done((shippingAddressData)=>{
  console.log(shippingAddressData);
});
```

### getDefaultBillingAddress

/**
 * Returns the current user's default billing address
 * @param  {[Cortex]} cortexInstance [The Cortex Instance in use]
 * @return {JQuery.promise}
 */

#### Example Usage

```
$.when(Accounts.getDefaultBillingAddress(cortexInstance)).done((shippingAddressData)=>{
  console.log(shippingAddressData);
  done();
});
```
## ParserModules/Cart.js

### shoppingCartParent

/**

* Queries cortex's current cart and returns the list of items in the cart

* @param  {[type]}   cortexInstance

* @return {JQuery.promise}
*/

### Example Usage

```
$.when(Cart.shoppingCartParent(cortexInstance)).done(
  (shoppingItemData)=>{
  console.log(shoppingItemData);
  done();
});
```


### getShoppingCartQuantity

/**

* Returns the amount of items within the current users cart

* @param  {Cortex} cortexInstance  the cortex instance containing the user token

* @return {JQuery.promise}

*/

#### Example Usage

```

$.when(Cart.getShoppingCartQuantity(cortexInstance)).done( (quantity) => {
  console.log(quantity);
});

```

### getShoppingCartTotal

/**

* gets the total cost of the current items inside the cart

* @param  {Cortex}   cortexInstance cortex object containing user token

* @param  {Function} successCallback the callback that runs when total is fetched successfully.  The total amount is passed to this callback

* @return {null}

*/

#### Example Usage

```

const successCallback = (total) => {
  console.log(total);
  done();
};

const failureCallback = (err) => {
  console.log(err);
  done();
};

Cart.getShoppingCartTotal(cortexInstance, successCallback, failureCallback);

```

### getPromotionDiscountForCart

/**

* fetches any applied discounts on the current cart either through promotions, discounts, or coupons

* @param  {Cortex} cortexInstance the cortex object containing the token

* @return {null}

*/

#### Example Usage

```

$.when(Cart.getPromotionDiscountForCart(cortexInstance)).done(
  (promotionDiscount) => {
    console.log(promotionDiscount);
  }
);

```

### applyPromoCode

/**

 * applies the promotional code to the current cart

 * @param  {String} cortexInstance - the cortex instance

 * @param  {String} postCode       - the promo code

 * @return {JQuery.promise}

 */

#### Example Usage

```
const promoCode = 'exampleCode';

$.when(Cart.applyPromoCode(cortexInstance, promoCode))
.done((promoCodeSuccess) => {
  console.log(promoCodeSuccess);
})
.fail((error) => {
  console.log(error);
});
```


## ParserModules/Checkout.js

### checkout

/**

* Function will checkout current cart in cortex

* @param  {[type]} cortexInstance

* @return {JQuery.promise}

*/

#### Example Usage

```
$.when(Checkout.checkout(cortexInstance))
.done( (cortexCheckoutData) => {
  console.log(cortexCheckoutData);
})
.fail((error)=>{
  console.log(error);
});
```

## ParserModules/Items.js

### getAllItems

/**

* getAllItems in root or specified category

* @param  {Cortex} cortexInstance cortex object instance with token

* @param  {String} categoryPath   leave this undefined or null for all items starting from the root.  To find items in a specific category then provide a path. EX. Mens/Shirts

* @return {jQuery.Deferred.Promise} returns a promise, when resolved will provide all items in categorical structure

*/

#### Example Usage

```

$.when(Items.getAllItems(cortexInstance, null)).done(
  (itemListData) => {
    console.log(itemListData);
  }
);

```

### getItemCategories

/**

* Provides all categories

* @param  {Cortex} cortexInstance cortex object containing token

* @param  {String} categoryPath   The category which we would like to see more subcategories of.  Ex. /Mens/Shirts or Ex. '/' for the entire category tree

* @return {JQuery.Deferred.Promise} returns a promise, once resolved returns an object with the category tree

*/

### Example Usage

```

$.when(Items.getItemCategories(cortexInstance, '/')).done(
  (categories) => {
    console.log(categories);
  }
)

```

### findProductAssets

/**

* given a cortex product link, the function will grab the asset url

* @param  {Cortex} cortexInstance cortex object containing token

* @param  {String} link           item link: ex. http://54.201.115.161:9080/cortex/items/mobee/qgqvhmluoq4tmnrqgaywc5s7onsf6ytvpe=

* @return {jQuery.Deferred.Promise} Returns the promise, when resolved the new Url is passed through                

*/

### Examples Usage

```

$.when(Items.findProductAssets(
  cortexInstance,
  'http://54.201.115.161:9080/cortex/items/mobee/qgqvhmluoq4tmnrqgaywc5s7onsf6ytvpe='
  )
)
.done((assets) => {
  console.log(assets);
});

```


### getItem

/**

* when called will provide the price of the object... NOTE this function is still WIP.  Should return all properties of product

* @param  {Cortex} cortexInstance Cortex object containing token

* @param  {String} sku            The sku of a particular product

* @return {JQuery.Deferred.Promise} Returns promise, when resolved provides pricing for object

*/

#### Example Usage

```

const skuCode = 'exampleSkuCode';

$when(Items.getItem(cortexInstance, skuCode))
.done((item) => {
    console.log(item);
});

```

### getItemChoices

/**
 * getItemChoices -- Finds other variants of the same product and returns the selection link
 * @param {Cortex} cortexInstance - Cortex Object
 * @param {String} sku - the sku code we would like to find variants for.
 */

#### Example Usage

```

$.when(Items.getItemChoices(cortexInstance, sku)).done(
  (itemChoices) => {
    console.log(itemChoices);
  }
);

```

### addToWishlist

/**
 * Will add a particular item to the wishlist based on sku
 * @param  {[type]} cortexInstance - the cortex instance
 * @param {[type]} sku - the Item sku
 */

#### Example Usage

```
const sku = 'Ur_Own_SKU';

$.when(Items.addToWishlist(cortexInstance, sku)).done(
  () => {
    // TODO: FILL OUT CALLBACK CODE
  }
).fail(
  (error) => {
    console.log(error);
  });
```

### getWishlistItems

/**
 * Will get the list of wishlist items
 * @param  {[type]} cortexInstance - the cortex instance
 * @return {JQuery.Deferred.Promise} Return a jquery promise, once resolved a JSON object is returned.
 */

#### Example Usage

```
$.when(Items.getWishlistItems(cortexInstance)).done(
  (wishListItems) => {
    console.log(wishListItems);
});
```

#### Example Output

```
{ gyywmobumi2daljvgrtdoljugyzdillcmnrwilldha4wkzjrmmydgyrtgi:
   { movetocartform: 'http://54.201.115.161:8080/cortex/wishlists/vestri/gztdgntemjtgeljvmm4dqljugnsteljyga2wmljvhe2daylbmy4wknzygm=/lineitems/gyywmobumi2daljvgrtdoljugyzdillcmnrwilldha4wkzjrmmydgyrtgi=/carts/form',
     wishlistItem:
      { displayName: 'Stainless Steel Water Bottle',
        details: [Array],
        sku: 'VESTRI_STAINLESS_STEEL_WATER_BOTTLE',
        price: [Object],
        assetUrl: null,
        bundle: null,
        category: null,
        availability: 'AVAILABLE' } },
  gvrgenzzgzstoljrhfrtkljugvtgmljygm4daljwgy2wcmzqhe2ggzrxhe:
   { movetocartform: 'http://54.201.115.161:8080/cortex/wishlists/vestri/gztdgntemjtgeljvmm4dqljugnsteljyga2wmljvhe2daylbmy4wknzygm=/lineitems/gvrgenzzgzstoljrhfrtkljugvtgmljygm4daljwgy2wcmzqhe2ggzrxhe=/carts/form',
     wishlistItem:
      { displayName: 'Women\'s Modena Leather Jacket',
        details: [Array],
        sku: 'VESTRI_WOMENS_MODENA_LEATHER_JACKET_BK_MD',
        price: [Object],
        assetUrl: null,
        bundle: null,
        category: null,
        availability: 'AVAILABLE' } },
  gm3wcyrshezggljwgbrwkljugi4gillbmjsdeljygvswen3dgzstqzrzmu:
   { movetocartform: 'http://54.201.115.161:8080/cortex/wishlists/vestri/gztdgntemjtgeljvmm4dqljugnsteljyga2wmljvhe2daylbmy4wknzygm=/lineitems/gm3wcyrshezggljwgbrwkljugi4gillbmjsdeljygvswen3dgzstqzrzmu=/carts/form',
     wishlistItem:
      { displayName: 'Women\'s CR550 Tee',
        details: [Array],
        sku: 'VESTRI_WOMENS_MODEL_X_TEE_WH_MD',
        price: [Object],
        assetUrl: null,
        bundle: null,
        category: null,
        availability: 'AVAILABLE' } } }
```
This JSONObject shows three items in the wishlist.  Each item contains a movetocartform (link that can be used to move that particular item to the cart from the wishlist) and the details of a particular item.


### getItemsByKeyword

/**
 * Will query keyword cortex resource
 * @param  {[Object]} cortexInstance - The cortex resource
 * @param  {[String]} keyword        - The keyword to be searched
 * @return {[JQuery.Deferred.Promise]} - Returns a jquery promise
 */

#### Example Usage

```
const keyword = 'your keyword';
$.when(Items.getItemsByKeyword(cortexInstance, keyword)).done(
  (keywordSearchResults) => {
    console.log(keywordSearchResults);
});
```

#### Example Output
```
[{
	"code": "VESTRI_GOLF_BAG",
	"details": [{
		"display-name": "Summary",
		"display-value": "Designed with a lightweight and durable construction, the Vestri Golf Bag has all the features you expect from a full-size cart bag. With 8 functional pockets, you can store all of the gear you need while you are out on the course. The standing legs, capped with gripping ends, keep your bag stable on the course. The bag features a ‘valuables’ waterproof pocket with ultra-soft fleece lining to keep your watch, wallet and other similar items safe. The double-strap system keeps you comfortable for hours by distributing the bag's weight evenly across your shoulders.",
		"name": "summary",
		"value": "Designed with a lightweight and durable construction, the Vestri Golf Bag has all the features you expect from a full-size cart bag. With 8 functional pockets, you can store all of the gear you need while you are out on the course. The standing legs, capped with gripping ends, keep your bag stable on the course. The bag features a ‘valuables’ waterproof pocket with ultra-soft fleece lining to keep your watch, wallet and other similar items safe. The double-strap system keeps you comfortable for hours by distributing the bag's weight evenly across your shoulders."
	}, {
		"display-name": "Tag",
		"display-value": "vestri:Accessories",
		"name": "tag",
		"value": ["vestri:Accessories"]
	}],
	"displayName": "Vestri Golf Bag",
	"listPrice": [{
		"amount": 175,
		"currency": "USD",
		"display": "$175.00"
	}],
	"purchasePrice": [{
		"amount": 175,
		"currency": "USD",
		"display": "$175.00"
	}]
}, {
	"code": "VESTRI_WOMENS_VESTRI_LOGO_POLO_BK_MD",
	"details": [{
		"display-name": "Summary",
		"display-value": "This shirt made from long grain, GMO free Peruvian cotton. Peru is the only country in the world where cotton is picked by hand on a large scale. The 4,500-year-old tradition prevents damage to the fiber during the picking process and removes the need to use chemicals to open the cotton plants before harvest. This environmentally friendly process results in cotton that is soft, strong, and lustrous – and the tee will get even softer with every wash.",
		"name": "summary",
		"value": "This shirt made from long grain, GMO free Peruvian cotton. Peru is the only country in the world where cotton is picked by hand on a large scale. The 4,500-year-old tradition prevents damage to the fiber during the picking process and removes the need to use chemicals to open the cotton plants before harvest. This environmentally friendly process results in cotton that is soft, strong, and lustrous – and the tee will get even softer with every wash."
	}, {
		"display-name": "Tag",
		"display-value": "vestri:Womens/Shirts",
		"name": "tag",
		"value": ["vestri:Womens/Shirts"]
	}],
	"displayName": "Women's Vestri Logo Polo",
	"listPrice": {},
	"purchasePrice": {}
}]
```
This output shows an array of two keyword searched items.




## ParserModules/Orders.js

### getOrders

}

### Example Usage

```

$.when(Orders.getOrders(cortexInstance)).done((orderData)=>{
  console.log(orderData);
  done();
});

```

## ParserModules/RecommendedItems.js

### findCrosssellProducts

/**

* finds all crosssell products

* @param  {Cortex} cortexInstance cortex instance containing a token

* @param  {productId} productId   the instance id of the product TODO: this needs to be changed to SKU

* @return {null}

*/

#### Example Usage

```
const sku = 'VESTRI_21_TURBINE_WHEEL_PACKAGE_GRAY';

$.when(RecommendedProducts.findCrosssellProducts(cortexInstance, sku))
.done((data) => {
  console.log(data);
}).fail((err) => {
  console.log(err);
})
```

### findRecommendedProducts

/**

* finds recommended products

* @param  {Cortex} cortexInstance Cortex object containing token

* @param  {String} productId      product instance id... TODO: should be changed to SKU.

* @return {null}

*/

#### Example Usage

```

const sku = 'VESTRI_MENS_SS_HANLEY_CH_LG';

$.when(RecommendedProducts.findRecommendedProducts(cortexInstance, sku)).done((recommendedProducts) => {
  console.log(recommendedProducts);
})
.fail((error) => {
  console.log(error);
});

```

## Cortex.js

### cortexRegister

/**

* Posts a user registration given user information

* @param  {String} email           the new user email to be registered

* @param  {String} password        the password of the new user

* @param  {String} firstName       the first name of the user

* @param  {String} lastName        the last name of the user

* @param  {Function} errorOnRegister the error function if registration fails

* @return {Object}                 returns a promise.  Resolved once server returns response

*  NOTE:  This request will only work if this Cortex Instance is anonymously logged in.  Highly suggested to use 'registerUser' in Accounts.js.

#### Example Usage
```

const email = 'curtis.yarn@ep.com';
const password = 'password';
const firstName = 'Curtis';
const lastName = 'Yarn';

const errorOnRegister = (error) => {
  console.log(error);
};

$.when(
  anonCortexInstance.cortexRegister(
    email, password, firstName, lastName, errorOnRegister)
    )
    .done((registrationData) => {
    console.log(registrationData);
});

```

*/


### cortexGetItem

/**

 * function gets item information

 * @param  {String} sku - the sku code of the item you would like to search

 * @param  {String} zoom - The attribute of the item you would like to zoom to.  EX.  can be 'price' or 'definition' or even 'recommendations:crosssell,price'

 * @return {JQuery.promise}

 */

#### Example Usage

```

const sku = '7854794_BLACK_XSM';

$.when(cortexInstance.cortexGetItem(sku, 'price,code,recommendations')).done((itemData) => {
  console.log(itemData);
  done();
});

```


### cortexAddToCart

/**

 * Adds the selected sku item to the signed in users cart

 * @param  {String} sku      - sku code of desired item to add to cart

 * @param  {Integer} quantity - the amount that should be added to cart

 * @return {[JQuery.promise]}  

 */

#### Example Usage

```

const sku = '7854794_BLACK_XSM';
const quantity = 1;

$.when(cortexInstance.cortexAddToCart(sku, quantity)).done((cortexSuccessMessage)=>{
  console.log(cortexSuccessMessage);
});

```


### cortexDeleteFromCart

/**

* deletes the current item from cart.

* @param  {String} sku - sku code of the item in the cart that would like to be deleted

* @return {JQuery.promise}

*/

#### Example Usage

```

const sku = 'VESTRI_WOMENS_MODEL_X_TEE_WH_MD';

$.when(cortexInstance.cortexDeleteFromCart(sku)).done((deleteData) => {
  console.log(deleteData);
});

```



### cortexShowPaymentSelection

/**

* function will show the current payment option selected

* @return {Object} - returns a jQuery promise.

*/

#### Example Usage

```

$.when(cortexInstance.cortexShowPaymentSelection()).done((paymentSelection) => {
  console.log(paymentSelection);
});

```

### cortexCreatePaymentMethod

/**

* Creates a new payment method selection for the user

* @param  {String} displayName  - Display name of the payment method

* @param  {String} paymentToken - tokenized payment information

* @return null

*/

#### Example Usage

```

$.when(cortexInstance.cortexCreatePaymentMethod(
  'VISA',
  '123423213'
)).done((paymentMethodSuccessMessage) => {
  console.log(paymentMethodSuccessMessage);
});

```

### cortexShowBillingAddresses

/**

* function will show the current billing address option selected

* @return {Object} - returns a jQuery promise.

*/

#### Example Usage

```

$.when(cortexInstance.cortexShowBillingAddresses()).done((addressSelection) => {
  console.log(addressSelection);
});

```


### cortexCreateAddress

/**

* Creates an address in the current account

* @param  {String} countryCode     - Ex. CA, US, UK, EU

* @param  {String} extendedAddress - Can be any string specifying more information

* @param  {String} locality        - City

* @param  {String} postalCode      - postalCode

* @param  {String} region          - State or Province

* @param  {String} streetAddress   - street address

* @param  {String} lastName        - last name

* @param  {String} firstName       - first name

* @return null

*/

#### Example Usage

```

$.when(cortexInstance.cortexCreateAddress(
  'US',
  '',
  'Richmond',
  '1234',
  'VA',
  '1234 Fake Street',
  'Murray',
  'Bill'
)).done((addressSuccessMessage) => {
  console.log(addressSuccessMessage);
});

```


## Testing the SDK standalone


### Overview of testing the SDK

Upon downloading or cloning the SDK, you can test each one of its functions through mocha (A javascript test framework).
All of the test files are located in the 'test' folder of this project.  Inside 'test/ParserModules' contains files testing each module available in the SDK.  


### How to start testing

Step 1: Open the file 'Config/default.json'

Step 2: Adjust the contents of the "testConfig" attributes to meet those of your current Cortex Instance

Step 3: Open terminal and navigate to your local copy of the SDK

Step 4: From the root directory of the project run a mocha command to test any one of the available SDK functions.
Here is the command: mocha "test/ParserModules/{MODULEFILENAME} -g {APIFUNCTION}

STEP 5: Check that the dependancies najax, jquery, and jsdom@9.12.0 have been downloaded as a dependancy as the project standalone does not include these dependancies within the package.json

NOTE:  You will need to change the {MODULEFILENAME} AND {APIFUNCTION} accordingly

Here is an example of how you would test the getAllItems API function:  mocha "test/ParserModules/Items.spec.js" -g 'getAllItems'
