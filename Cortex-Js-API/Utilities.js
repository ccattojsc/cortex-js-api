// ***************************************************************
// Copyright © 2018 Elastic Path Software Inc. All rights reserved.
// ***************************************************************
let $ = null;

try {
  // TODO: Only bring in JSDOM and NAJAX if the user says they are going to use the libs server side.
  const najax = require('najax');
  const jsdom = require('jsdom').jsdom;
  const doc = jsdom();
  const window = doc.defaultView;
  $ = require('jquery')(window);
  $.ajax = najax;
} catch (error) {
  $ = require('jquery');
}

const Cortex = require('./Cortex.js');

let CognitoCortex = null;
try {
  CognitoCortex = require('cortex-cognito').CognitoCortex;
} catch (ex) {
    console.log('cortex-cognito unavailable');
}

'use strict';

// TODO: Make global error function
var errorHandler = function errorHandler(error) {
  console.log('error');
  console.log(error);
};

// TOOD: Need to move this to Profile!!!
function cortexGetDeliveryChoices(cortexInstance, callback) {
  var zoomUrl = cortexInstance.cortexBaseUrl + '/?zoom=';
  var zoom = 'defaultcart:order:deliveries:element:shippingoptioninfo:selector';
  $.when(cortexInstance.cortexGetZoomData(zoomUrl, zoom)).done(function (data) {
    var loChoices = data._defaultcart[0]._order[0]._deliveries[0]._element[0]._shippingoptioninfo[0]._selector[0].links;
    $.when(createOptionsJson(cortexInstance, loChoices, 'deliveryOptions')).done(function (jsonData) {
      callback(jsonData);
    });
  });
}


function parseRelevantDeliveryInfo(data, obj) {
  var deliveryObj = {};
  deliveryObj.carrier = data.carrier;
  deliveryObj.cost = data.carrier;
  deliveryObj.displayName = data['display-name'];
  deliveryObj.name = data.name;

  var returnObj = obj;
  returnObj.delivery = deliveryObj;

  return returnObj;
}

function parseRelevantAddressInfo(data, obj) {
  var addressObj = {};
  addressObj = data.address;
  addressObj.name = data.name;

  var returnObj = obj;
  returnObj.address = addressObj;

  return returnObj;
}

/**
 * Will choose the right format and parse the relevant information
 * @param  {[type]} typeOfOptions Tells the function what kind of object it needs to parse.
 * @return returns an object with relevant information
 */
function parseRelevantSelectionData(data, obj, typeOfOptions) {
  var returnObj = void 0;

  switch (typeOfOptions) {
  case 'address':
    returnObj = parseRelevantAddressInfo(data, obj);
    break;
  case 'deliveryOptions':
    returnObj = parseRelevantDeliveryInfo(data, obj);
    break;
  case 'displayName':
    returnObj = parseDisplayName(data, obj);
    break;
  default:
    console.log('no options found in parseRelevantSelectionData');
  }
  return returnObj;
}

function createOptionsJson(cortexInstance, loChoices, typeOfOptions) {
  var dfd = $.Deferred();
  var arrayOfAddresses = [];
  // We minus 1 here because the last link is not a choice parameter
  var lengthOfPromises = loChoices.length; // NOTE: We have gotten rid of the minus one and it may break aything outside of the Gallo demo
  var resolvedPromiseCount = 0;

  loChoices.forEach(function (choice) {
    var href = choice.href;

    var callbackForAddresses = function callbackForAddresses(choiceData) {
      var data = cortexInstance.convertToObj(choiceData);
      var links = data.links;
      var obj = {};
      obj.status = choice.rel;
      links.forEach(function (link) {
        if (link.rel == 'description') {

          $.when(getDescriptionOfChoice(cortexInstance, link.href)).done(function (descriptionData) {
            var data = cortexInstance.convertToObj(descriptionData);
            obj = parseRelevantSelectionData(data, obj, typeOfOptions);
            arrayOfAddresses.push(obj);
            resolvedPromiseCount += 1;

            if (resolvedPromiseCount === lengthOfPromises) {
              dfd.resolve(arrayOfAddresses);
            }
          });
        }
        if (link.rel === 'selectaction') {
          obj.selectAction = link.href;
        }
      });
    };
    var errorHandler = function errorHandler(error) {
      console.log('error when creating shipping address choice Json:' + error);
    };
    cortexInstance.cortexGet(href, callbackForAddresses, errorHandler);
  });
  return dfd.promise();
}

function getDescriptionOfChoice(cortexInstance, href) {
  var dfd = $.Deferred();
  var success = function success(data) {
    dfd.resolve(data);
  };
  var error = function error(err) {
    console.log(err);
  };
  cortexInstance.cortexGet(href, success, error);
  return dfd.promise();
}
// --------------------------------------------------> shipping address select / billing address select

// ---------------------------> payment parser
/**
 * [createAnonCortexInstance - creates an anonymous login cortexInstance.]
 * @param  {[String]} baseUrl [the instance of Cortex]
 * @param  {[String]} scope   [the desired scope]
 * @return {[Cortex]}         [an Anonymouse Cortex Object]
 */
function createAnonCortexInstance(baseUrl, scope) {
  var dfd = $.Deferred();

  var cortexInstance = new Cortex(baseUrl, scope);

  var success = function success(loginData) {
    var data = cortexInstance.convertToObj(loginData);
    cortexInstance.token = data.access_token;
    dfd.resolve(cortexInstance);
  };

  var error = function error(err) {
    console.log('error logging in');
    console.log(err);
  };

  cortexInstance.cortexAnonLogin(success, error);

  return dfd.promise();
}

/**
 * [createCortexInstance - Used to return an authenticated cortex instance.]
 * @param  {[String]} username [the username of EPSHOPPER]
 * @param  {[String]} password [the password of EPSHOPPER]
 * @param  {[String]} baseUrl  [the baseUrl of your Cortex instance ex. http://35.163.108.181:8080/cortex]
 * @param  {[String]} scope    [the scope of EPSHOPPER]
 * @return {[Cortex]}          [authenticated cortex instance]
 */
function createCortexInstance(username, password, baseUrl, scope) {
  var dfd = $.Deferred();
  var cortexInstance = new Cortex(baseUrl, scope);
  var success = function success(loginData) {
    console.log('cortex login worked!!');
    var data = cortexInstance.convertToObj(loginData);
    cortexInstance.token = data.access_token;
    dfd.resolve(cortexInstance);
  };

  var error = function error(err) {
    console.log('error logging in');
    console.log(err);
    dfd.reject('Unable to Login');
  };
  cortexInstance.cortexLogin(username, password, success, error);

  return dfd.promise();
}

/**
 * [createCognitoCortexInstance Function will create a Cognito cortex instance that is equipped to make calls to Cortex in Trusted header mode]
 * @param  {[type]} username        [The username of the user to be placed in header]
 * @param  {[type]} password        [The password of the user to be placed in header]
 * @param  {[type]} cognitoInstance [The Cognito Instance instantiated, containing configuration data for your current set up]
 * @return {[CognitoCortex]}                 [the CognitoCortex object]
 */
function createCognitoCortexInstance(username, password, cognitoInstance) {
  var dfd = $.Deferred();
  const authenticateUserCallback = (result) => {
    const idToken = result.getIdToken().getJwtToken();
    const headerData = cognitoInstance.extractHeadersFromToken(idToken);

    const url = cognitoInstance.apiGateWayUrl;

    let cognitoCortexInstance = null;
    if (CognitoCortex != null) {
      cognitoCortexInstance = new CognitoCortex(url, headerData.scope, idToken, headerData);
    }

    dfd.resolve(cognitoCortexInstance);
  };

  const authenticateUserErrorCallback = (err) => {
    dfd.reject(err);
  }

  cognitoInstance.authenticateUser(
    username,
    password,
    authenticateUserCallback,
    authenticateUserErrorCallback
  );

  return dfd.promise();
}

/**
 * create call to select a flavour preference.
 */
function parseDisplayName(data, obj) {
  var returnObj = obj;
  returnObj.displayName = data['display-name'];

  return returnObj;
}

/**
 * Function will count the # elements and next elements in the array.
 * @param  {[type]} links [takes in a list of links]
 * @return {[type]}       [outputs the number of elements there are in the links]
 */
function findNumberOfElements(links) {
  let counter = 0;
  links.forEach((link) => {
    if (link.rel === 'element') {
      counter += 1;
    }
    if (link.rel === 'next') {
      counter += 1;
    }
  });
  return counter;
};

function getTodaysDate(){
  const today = new Date();
  const dd = today.getDate();
  const mm = today.getMonth() + 1;
  const yyyy = today.getFullYear();
  return `${mm} / ${dd} / ${yyyy}`;
};

String.prototype.replaceAll = function(search, replace) {
  if (replace === undefined) {
    return this.toString();
  }
  return this.replace(new RegExp('[' + search + ']', 'g'), replace);
};


var convertToObj = function convertToObj(data) {
  if (typeof data === 'string') {
    if (data !== null && data.trim() !== '') {
      return JSON.parse(data);
    }
  }
  return data;
};

module.exports.convertToObj = convertToObj;
module.exports.getTodaysDate = getTodaysDate;
module.exports.findNumberOfElements = findNumberOfElements;
module.exports.createAnonCortexInstance = createAnonCortexInstance;
module.exports.createCortexInstance = createCortexInstance;
module.exports.createCognitoCortexInstance = createCognitoCortexInstance;
