// ***************************************************************
// Copyright © 2018 Elastic Path Software Inc. All rights reserved.
// ***************************************************************
const config = require('config');

const Cognito = require('cortex-cognito').Cognito;
const cognitoInstance = new Cognito();

var cognitoUsers = config.get('cognitoUsers');
console.log(cognitoUsers);

var size = Object.keys(cognitoUsers).length;
console.log(size);

for (const userNumber in cognitoUsers) {
  var userObj = cognitoUsers[userNumber];

  const signupSuccessCallback = (err, result) => {
    if (err) {
      console.log(err);
      return;
    }
    const cognitoUser = result.user;
    console.log(`user name is ${cognitoUser.getUsername()}`);

    // decrement and see if we should return the test.
    size -= 1

    if (size == 0) {
      done();
    }
  };

  cognitoInstance.createCognitoUser(
    userObj.email,
    userObj.password,
    userObj.phoneNumber,
    userObj.fullname,
    userObj.userId,
    userObj.role,
    userObj.scope,
    signupSuccessCallback,
  );
}
