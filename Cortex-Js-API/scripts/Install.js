// ***************************************************************
// Copyright © 2018 Elastic Path Software Inc. All rights reserved.
// ***************************************************************

var prompt = require('prompt');
var colors = require("colors/safe");
var npm = require('npm');
//
// Setting these properties customizes the prompt.
//
prompt.delimiter = colors.green("><");

prompt.start();

// TODO: Ask all the questions before... then downloading everything at the same time.

// This should prompt when the user wants to start changing there testing parameters.
(function startSetupPrompt() {
  // TODO: ask to first fill out the test values... Or we can ask that when he runs the test script once instead...
  startCognitoPrompt();
})();

let npmModuleDownloadArray = [];

function startCognitoPrompt() {

  prompt.message = '';

  var cognitoMessage = colors.magenta("Would you like to add dependancies necessary for Cognito Integration? ") + 'input y or n';

  prompt.get({
      properties: {
        isCognito: {
          description: cognitoMessage
        }
      }
    }, function (err, result) {

      if (result.isCognito === 'y') {
        console.log(colors.cyan("Pulling in Cognito Dependancies..."));
        npmModuleDownloadArray.push('@elasticpathtechsales/cortex-cognito');
        startServerOrBrowserConfigurationPrompt();
      } else if (result.isCognito === 'n') {
        console.log(colors.red("Cognito Dependancies will not be downloaded..."));
        startServerOrBrowserConfigurationPrompt();
      } else {
        console.log('Invalid input.  Please answer with y or n.');
        startCognitoPrompt();
      }
    });
}

function startDependancyDownloads() {
  if (npmModuleDownloadArray.length != 0) {
    npm.load(function(err) {
      // HAX: We may need to install the beta version here.
      npm.commands.install(npmModuleDownloadArray, function(er, data) {
        console.log(colors.cyan("Install Succeeded"));
      });
    });
  }
}

function startServerOrBrowserConfigurationPrompt() {
  // TODO: Ask the user whether or not he will be using this server side or client side... Depending on the answer download najax or jsdom..
  prompt.message = '';

  var message = colors.magenta("Are you planning to use the SDK on the server side or browser side? ") + 'input server or browser';

  prompt.get({
      properties: {
        usage: {
          description: message
        }
      }
    }, function (err, result) {
      if (result.usage === 'server') {
        console.log(colors.cyan("Pulling in server side dependancies"));
        // TODO: start to download najax and jsdom...
        npmModuleDownloadArray.push('jsdom@9.12.0');
        npmModuleDownloadArray.push('najax');
        startDependancyDownloads();
      } else if (result.usage === 'browser') {
        console.log(colors.red("Cognito Dependancies will not be downloaded..."));
        startDependancyDownloads();
      } else {
        console.log('Invalid input.  Please answer with server or browser.');
        startServerOrBrowserConfigurationPrompt();
      }
    });
}

// TODO: Fill this cognitoConfigurationPrompt.
function startCognitoConfigurationPrompt() {
  // console.log(colors.green('Enter Cognito configuration data'));
  // prompt.message = '';
  //
  // prompt.get(['apiGateWayUrl', 'UserPoolId', 'ClientId', 'accessKeyId', 'secretAccessKey', 'region'], function (err, result) {
  //     setConfigValues(result);
  //     startCognitoDependancyDownload();
  //   });
  startCognitoDependancyDownload();
}

function setConfigValues(result) {
  // BUG: This does not alter the configuration file... Must be because it is set to immutable.  Might not need this here.
  process.env.NODE_CONFIG = JSON.stringify({
    cognitoConfig: {
      apiGateWayUrl: result.apiGateWayUrl,
      UserPoolId: result.UserPoolId,
      ClientId: result.ClientId,
      accessKeyId: result.accessKeyId,
      secretAccessKey: result.secretAccessKey,
      region: result.region
    }
  });

  console.log('printing the altered environment variable');
  console.log(process.env.NODE_CONFIG);
}
