// ***************************************************************
// Copyright © 2018 Elastic Path Software Inc. All rights reserved.
// ***************************************************************
var settings = {
  "useCortexForAssets": false,
  "S3AssetUrlPrefix": "https://s3-us-west-2.amazonaws.com/vestri-catalog-images/",

  "getRequestTimeout": 5000,
  "postRequestTimeout": 5000,

  "testConfig": {
    "instanceIP": "http://34.217.42.119:8080/cortex",
    "EPShopperUsername": "trevor.linden@ep.com",
    "EPShopperPassword": "password",
    "role": "REGISTERED",
    "scope": "vestri",
    "isCognitoMode": false
  },

  "cognitoConfig": {
    "apiGateWayUrl": "https://uj64zqmrqa.execute-api.us-west-2.amazonaws.com/Test/cortex",
    "UserPoolId": "us-west-2_2SAeR0teM",
    "ClientId": "59pqjai9nq1pqg78b8hifl6rpq",
    "accessKeyId": "",
    "secretAccessKey": "",
    "region": "us-west-2"
  },

  "cognitoUsers": {
    "1": {
      "email": "james.bond@ep.com",
      "password": "password",
      "phoneNumber": "+17782466669",
      "fullname": "James Bond",
      "userId": "james.bond@ep.com",
      "role": "PUBLIC",
      "scope": "JWinery"
    },
    "2": {
      "email": "daniel.hegwick@ep.com",
      "password": "password",
      "phoneNumber": "+17782466669",
      "fullname": "Daniel Hegwick",
      "userId": "daniel.hegwick@ep.com",
      "role": "PUBLIC",
      "scope": "JWinery"
    },
    "3": {
      "email": "barbara.henson@ep.com",
      "password": "password",
      "phoneNumber": "+17782466669",
      "fullname": "Barbara Henson",
      "userId": "barbara.henson@ep.com",
      "role": "PUBLIC",
      "scope": "JWinery"
    },
    "4": {
      "email": "zoey.burke@ep.com",
      "password": "password",
      "phoneNumber": "+17782466669",
      "fullname": "Zoey Burke",
      "userId": "zoey.burke@ep.com",
      "role": "PUBLIC",
      "scope": "JWinery"
    },
    "5": {
      "email": "john.lennon@ep.com",
      "password": "password",
      "phoneNumber": "+17782466669",
      "fullname": "John Lennon",
      "userId": "john.lennon@ep.com",
      "role": "PUBLIC",
      "scope": "JWinery"
    },
    "6": {
      "email": "jim.harris@ep.com",
      "password": "password",
      "phoneNumber": "+17782466669",
      "fullname": "Jim Harris",
      "userId": "jim.harris@ep.com",
      "role": "PUBLIC",
      "scope": "JWinery"
    },
    "7": {
      "email": "paul.mccartney@ep.com",
      "password": "password",
      "phoneNumber": "+17782466669",
      "fullname": "Paul Mccartney",
      "userId": "paul.mccartney@ep.com",
      "role": "PUBLIC",
      "scope": "JWinery"
    },
    "8": {
      "email": "lynn.shirley@ep.com",
      "password": "password",
      "phoneNumber": "+17782466669",
      "fullname": "Lynn Shirley",
      "userId": "lynn.shirley@ep.com",
      "role": "PUBLIC",
      "scope": "JWinery"
    },
    "9": {
      "email": "karlie.kloss@ep.com",
      "password": "password",
      "phoneNumber": "+17782466669",
      "fullname": "Karlie Kloss",
      "userId": "karlie.kloss@ep.com",
      "role": "PUBLIC",
      "scope": "JWinery"
    },
    "10": {
      "email": "thomas.jefferson@ep.com",
      "password": "password",
      "phoneNumber": "+17782466669",
      "fullname": "Thomas Jefferson",
      "userId": "thomas.jefferson@ep.com",
      "role": "PUBLIC",
      "scope": "JWinery"
    },
    "11": {
      "email": "andres.fajardo@ep.com",
      "password": "password",
      "phoneNumber": "+17782466669",
      "fullname": "Andres Fajardo",
      "userId": "andres.fajardo@ep.com",
      "role": "PUBLIC",
      "scope": "JWinery"
    },
    "12": {
      "email": "lee.trotter@ep.com",
      "password": "password",
      "phoneNumber": "+17782466669",
      "fullname": "Lee Trotter",
      "userId": "lee.trotter@ep.com",
      "role": "PUBLIC",
      "scope": "JWinery"
    }
  }
};
module.exports.settings = settings;
