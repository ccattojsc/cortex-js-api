// ***************************************************************
// Copyright © 2018 Elastic Path Software Inc. All rights reserved.
// ***************************************************************
let $ = null;

try {
  // TODO: Only bring in JSDOM and NAJAX if the user says they are going to use the libs server side.
  const najax = require('najax');
  const jsdom = require('jsdom').jsdom;
  const doc = jsdom();
  const window = doc.defaultView;
  $ = require('jquery')(window);
  $.ajax = najax;
} catch (error) {
  $ = require('jquery');
}

function findLineItemFromList(purchaseLinks) {
  for (let i = 0; i < purchaseLinks.length; i++) {
    const currentPurchaseAttrLink = purchaseLinks[i];
    if (currentPurchaseAttrLink.rel === 'lineitems') {
      return currentPurchaseAttrLink;
    }
  }
  return null;
}

function createPurchaseLineItemObj(loElements) {
  const links = loElements._element;
  const returnArray = [];
  for (let i = 0; i < links.length; i++) {
    const currentElement = links[i];
    const lineExtensionTotal = currentElement['line-extension-amount'][0];
    const lineItemName = currentElement.name;
    const lineItemQuantity = currentElement.quantity;

    const returnObj = {
      total: lineExtensionTotal,
      name: lineItemName,
      quantity: lineItemQuantity
    };
    returnArray.push(returnObj);
  }

  return returnArray;
}

function getLineItemsFromPurchase(cortexInstance, currentPurchaseElement) {
  const dfd = $.Deferred();
  const purchaseLinks = currentPurchaseElement.links;
  const lineItemCortexObj = findLineItemFromList(purchaseLinks);
  const href = lineItemCortexObj.href;

  $.when(cortexInstance.cortexGetPurchaseLineItems(href)).done(
    (returnData) => {
      const lineItemObj = createPurchaseLineItemObj(returnData);
      dfd.resolve(lineItemObj);
    }
  );
  return dfd.promise();
}

function parseListOfPaymentElements(cortexInstance, purchaseElements) {
  const dfd = $.Deferred();
  const purchaseArray = [];
  let promisesReturned = 0;
  for (let i = 0; i < purchaseElements.length; i++) {
    const currentPurchaseElement = purchaseElements[i];
    $.when(getLineItemsFromPurchase(cortexInstance, currentPurchaseElement)).done(
      (lineItems) => {
        const purchaseObj = {};
        promisesReturned++;
        purchaseObj.lineItems = lineItems;
        purchaseObj.monetaryTotal = currentPurchaseElement['monetary-total'];
        purchaseObj.purchaseDate = currentPurchaseElement['purchase-date'];
        purchaseObj.purchaseNumber = currentPurchaseElement['purchase-number'];
        purchaseObj.taxTotal = currentPurchaseElement['tax-total'];
        purchaseObj.status = currentPurchaseElement.status;
        purchaseArray.push(purchaseObj);
        if (promisesReturned === purchaseElements.length) {
          dfd.resolve(purchaseArray);
        }
      }
    );
  }
  return dfd.promise();
}

/**
* get all orders for a particular user
* @param  {Cortex} cortexInstance  Cortex Instance containing token
* @return {null}
*/
function getOrders(cortexInstance) {
  const dfd = $.Deferred();

  $.when(cortexInstance.cortexGetOrders()).done(
    (orderData) => {
      const purchaseElements = orderData._defaultprofile[0]._purchases[0]._element;

      $.when(parseListOfPaymentElements(cortexInstance, purchaseElements)).done(
        (parsedPurchaseElements) => {
          const dataObj = cortexInstance.convertToObj(parsedPurchaseElements);
          dfd.resolve(dataObj);
        }
      );
    }
  );

  return dfd.promise();
}

module.exports.getOrders = getOrders;
