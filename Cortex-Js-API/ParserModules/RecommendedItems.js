// ***************************************************************
// Copyright © 2018 Elastic Path Software Inc. All rights reserved.
// ***************************************************************
let $ = null;

try {
  // TODO: Only bring in JSDOM and NAJAX if the user says they are going to use the libs server side.
  const najax = require('najax');
  const jsdom = require('jsdom').jsdom;
  const doc = jsdom();
  const window = doc.defaultView;
  $ = require('jquery')(window);
  $.ajax = najax;
} catch (error) {
  $ = require('jquery');
}

const Utilities = require('./../Utilities.js');
const Items = require('./Items.js');

// TODO: UNTESTED - need to test with the Vestri store where there are recommended products.

/**
 * function is used to grab all attributes pertaining to a particular Cortex item.
 * @param  Cortex cortexInstance
 * @param  [] links - list of items where the item link is located in array.href
 * @return [Object] - returns an array of objects, each object containing item specific data
 */
function getProductProperties(cortexInstance, links) {
  const dfd = $.Deferred();
  const linkLength = Utilities.findNumberOfElements(links);

  let returnCount = 0;
  const listOfRecommendedProducts = [];
  links.forEach((link) => {
    if (link.rel === 'element') {
      $.when(
          Items.findProductDefinition(cortexInstance, link.href),
          Items.findProductCode(cortexInstance, link.href),
          Items.findProductPrice(cortexInstance, link.href),
          Items.findProductAssets(cortexInstance, link.href)
        ).done(
          (definition, code, price, assets) => {
            const returnDefinition = cortexInstance.convertToObj(definition);
            returnDefinition.sku = code;
            returnDefinition.price = price;
            returnDefinition.assetUrl = assets;
            listOfRecommendedProducts.push(returnDefinition);
            returnCount += 1;
            if (returnCount === linkLength) {
              dfd.resolve(listOfRecommendedProducts);
            }
          }
        ).fail(
          (error) => {
            returnCount += 1;
            if (returnCount === linkLength) {
              dfd.resolve(listOfRecommendedProducts);
            }
          });
    }

    if (link.rel === 'next'){
      const nextCallback = (data) => {
        const nextData = cortexInstance.convertToObj(data);
        $.when(getProductProperties(cortexInstance, nextData.links)).done((nextPageProductProperties) => {
          listOfRecommendedProducts.concat(nextPageProductProperties);
          returnCount += 1;
          if (returnCount === linkLength) {
            dfd.resolve(listOfRecommendedProducts);
          }
        })
      }
      const error = (error) => {
        console.log('error trying to get the next pagination in recommmended items');
        console.log(error);
      }
      cortexInstance.cortexGet(
        link.href,
        nextCallback,
        error
      );
    }
  });
  return dfd.promise();
}


String.prototype.replaceAll = function(search, replace) {
  if (replace === undefined) {
    return this.toString();
  }
  return this.replace(new RegExp('[' + search + ']', 'g'), replace);
};

/**
 * [findRecommendedProducts - Finds the recommended products when given a product sku]
 * @param  {Cortex} cortexInstance Cortex object containing token
 * @param  {String} sku      The sku you would like to find recommended products for
 * @return {null}
 */
function findRecommendedProducts(cortexInstance, sku) {
  const dfd = $.Deferred();

  $.when(cortexInstance.cortexFindInstanceIdFromSku(sku)).done((instanceId) => {
    const success = (data) => {
      const dataObj = cortexInstance.convertToObj(data);
      dfd.resolve(dataObj);
    };
    const error = (err) => {
      dfd.reject(err);
    }
    findRecommendedProductsHelper(cortexInstance, instanceId, success, error);
  });

  return dfd.promise()
}

/**
 * [findRecommendedProductsHelper - Finds the recommended product when given a product instanceID]
 * @param  {Cortex} cortexInstance Cortex object containing token
 * @param  {String} productId      product instance id you would like to find recommended products for
 * @return {null}
 */
function findRecommendedProductsHelper(cortexInstance, productId, success, error) {
  const suffixUrl = `/items/${cortexInstance.scope}/${productId}`;
  const prefixUrl = cortexInstance.cortexBaseUrl;
  const zoom = '=?zoom=recommendations:recommendation';
  const url = prefixUrl + suffixUrl + zoom;
  const parseCallback = function (data) {
    data = cortexInstance.convertToObj(data);
    const links = data._recommendations[0]._recommendation[0].links;
    $.when(getProductProperties(cortexInstance, links)).done(
        (listOfRecommendedProducts) => {
          success(listOfRecommendedProducts);
        }
      );
  };
  cortexInstance.cortexGet(
    url,
    parseCallback,
    error
  );
}

/**
 * finds all crosssell products when given a sku.
 * @param  {Cortex} cortexInstance cortex instance containing a token
 * @param  {String} sku   The sku of the product you would like to find crosssell items for
 * @param  {Function} success        the success callback is called with json structure containing item properties
 * @param  {Function} error          failure callback
 * @return {null}
 */
function findCrosssellProducts(cortexInstance, sku) {
  const dfd = $.Deferred();

  $.when(cortexInstance.cortexFindInstanceIdFromSku(sku)).done((instanceId) => {
    const success = (data) => {
      const dataObj = cortexInstance.convertToObj(data);
      dfd.resolve(dataObj);
    }
    const error = (err) => {
      dfd.reject(err);
    }
    findCrosssellProductsHelper(cortexInstance, instanceId, success, error);
  });

  return dfd.promise();
}

/**
 * finds all crosssell products when given the instanceId
 * @param  {Cortex} cortexInstance cortex instance containing a token
 * @param  {String} productId   the instance id of the product you would like to find crosssell items for
 * @param  {Function} success        the success callback is called with json structure containing item properties
 * @param  {Function} error          failure callback
 * @return {null}
 */
function findCrosssellProductsHelper(cortexInstance, productId, success, error) {
  // TODO: Change product ID to accept sku instead of productId.
  const suffixUrl = `/items/${cortexInstance.scope}/${productId}`;
  const prefixUrl = cortexInstance.cortexBaseUrl;
  const zoom = '=?zoom=recommendations:crosssell';
  const url = prefixUrl + suffixUrl + zoom;
  const parseCallback = function (data) {
    data = cortexInstance.convertToObj(data);
    const links = data._recommendations[0]._crosssell[0].links;
    $.when(getProductProperties(cortexInstance, links)).done(
      (listOfRecommendedProducts) => {
        success(listOfRecommendedProducts);
      }
    );
  };
  cortexInstance.cortexGet(
    url,
    parseCallback,
    error
  );
}

module.exports.findCrosssellProducts = findCrosssellProducts;
module.exports.findRecommendedProducts = findRecommendedProducts;
