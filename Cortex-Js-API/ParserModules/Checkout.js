// ***************************************************************
// Copyright © 2018 Elastic Path Software Inc. All rights reserved.
// ***************************************************************
let $ = null;

try {
  // TODO: Only bring in JSDOM and NAJAX if the user says they are going to use the libs server side.
  const najax = require('najax');
  const jsdom = require('jsdom').jsdom;
  const doc = jsdom();
  const window = doc.defaultView;
  $ = require('jquery')(window);
  $.ajax = najax;
} catch (error) {
  $ = require('jquery');
}

const Cart = require('./Cart.js');

// TODO: Make global error function
const errorHandler = (error) => {
  console.log('error');
  console.log(error);
};

function getListOfLineItems(cortexInstance, href, obj) {
  const dfd = $.Deferred();
  const success = (lineItemData) => {
    const data = cortexInstance.convertToObj(lineItemData);
    const lineItemLinks = data.links;
    obj.lineItems = [];
    let i = 1;
    lineItemLinks.forEach(
      (link) => {
        if (link.rel === 'element') {
          const lineItemCallback = (lineItemData) => {
            const itemData = cortexInstance.convertToObj(lineItemData);
            let lineExtensionAmount = itemData['line-extension-amount'][0].display;
            lineExtensionAmount = lineExtensionAmount.replace(/,/g, '');
            const lineExtensionTax = itemData['line-extension-tax'][0].display;
            const lineExtensionTotal = itemData['line-extension-total'][0].display;

            const lineItemObj = {
              lineExtensionAmount,
              lineExtensionTax,
              lineExtensionTotal,
              quantity: data.quantity,
              name: data.name
            };

            obj.lineItems.push(lineItemObj);
            i += 1;

            if (lineItemLinks.length === i) {
              dfd.resolve(obj);
            }
          };
          const href = link.href;
          cortexInstance.cortexGet(href, lineItemCallback, errorHandler);
        }
      }
    );
  };

  cortexInstance.cortexGet(
    href,
    success,
    errorHandler
  );
  return dfd.promise();
}

function checkout(cortexInstance) {
  const dfd = $.Deferred();

  const successCallback = (data) => {
    const dataObj = cortexInstance.convertToObj(data);
    dfd.resolve(dataObj);
  }

  const errorCallback = (error) => {
    dfd.reject(error);
  }

  $.when(Cart.isCartEmpty(cortexInstance)).done(
    (isCartEmpty) => {
      if (isCartEmpty) {
        errorCallback('Empty Cart!');
      } else {
        cortexCheckoutHelper(cortexInstance, successCallback, errorCallback);
      }
    }
  );

  return dfd.promise();
}

function cortexCheckoutHelper(cortexInstance, successCallback, errorCallback) {
  $.when(cortexInstance.cortexCheckout()).done(
    (cortexCheckoutData) => {
      const data = cortexInstance.convertToObj(cortexCheckoutData);
      const obj = {};
      obj.monetaryTotal = data['monetary-total'][0];
      obj.purchaseDate = data['purchase-date']['display-value'];
      obj.purchaseCode = data['purchase-number'];
      obj.taxTotal = data['tax-total'];
      obj.status = data.status;

      const links = data.links;
      links.forEach(
        (link) => {
          if (link.rel === 'lineitems') {
            const href = link.href;
            $.when(getListOfLineItems(cortexInstance, href, obj)).done(
              (data) => {
                successCallback(obj);
              }
            );
          }
        }
      );
    }
  ).fail(
    (error) => {
      errorCallback(error);
    }
  );
}

module.exports.checkout = checkout;
