// ***************************************************************
// Copyright © 2018 Elastic Path Software Inc. All rights reserved.
// ***************************************************************
let $ = null;

try {
  // TODO: Only bring in JSDOM and NAJAX if the user says they are going to use the libs server side.
  const najax = require('najax');
  const jsdom = require('jsdom').jsdom;
  const doc = jsdom();
  const window = doc.defaultView;
  $ = require('jquery')(window);
  $.ajax = najax;
} catch (error) {
  $ = require('jquery');
}

const Items = require('./Items.js');

function getLinkData(cortexInstance, href) {
  const dfd = $.Deferred();

  cortexInstance.cortexGet(
    href,
    (data) => {
      dfd.resolve(data);
    },
    (error) => {
      console.log(error);
    }
  );
  return dfd.promise();
}

function extractItemVariants(cortexInstance, itemID) {
  const suffixUrl = `${cortexInstance.cortexBaseUrl}/itemdefinitions/${cortexInstance.scope}/${itemID}=/options`;
  const zoom = '?zoom=element:selector:chosen:description';
  const url = suffixUrl + zoom;
  const dfd = $.Deferred();
  cortexInstance.cortexGet(url, (data) => {
    dfd.resolve(data);
  },
        (error) => {
          console.log(error);
        }
     );
  return dfd.promise();
}

function extractItemDetails(itemDetails) {
  const details = itemDetails._code[0]._item[0]._definition[0].details[0];
  const displayName = itemDetails._code[0]._item[0]._definition[0]['display-name'];
  const obj = {};
  obj.name = displayName;
  obj.details = details;
  return obj;
}

// // We need to use this class for the line items in the purchase as well...
// // TODO: use this json creater for creating another purchase...
function createLineItemJSONFromCortex(avail, price, total, itemDetails, itemSelection, itemCode, assetUrl) {
  const returnJSON = {};
  const availability = avail.state;
  returnJSON.availability = availability;

  const purchasePrice = price['purchase-price'][0];
  returnJSON.purchasePrice = purchasePrice;

  const listPrice = price['list-price'][0];
  returnJSON.listPrice = listPrice;

  var total = total.cost[0];
  returnJSON.total = total;

  var itemDetails = extractItemDetails(itemDetails);
  returnJSON.itemDetails = itemDetails;

  const itemSKU = itemCode._code[0].code;
  returnJSON.itemDetails.productCode = itemSKU;

  returnJSON.assetUrl = assetUrl;

  return returnJSON;
}

function executeAllCartJobs(cortexInstance, hrefJobs) {
  const dfd = $.Deferred();
  let itemId = '';
  const n = hrefJobs.item.lastIndexOf('/');
  if (n != 0 || n != undefined) {
    itemId = hrefJobs.item.substring(n + 1);
  }
  $.when(
    getLinkData(cortexInstance, hrefJobs.availability),
    cortexInstance.cortexGetZoomData(`${hrefJobs.item}=?zoom=`, 'code:item:definition'),
    getLinkData(cortexInstance, hrefJobs.price),
    getLinkData(cortexInstance, hrefJobs.total),
    extractItemVariants(cortexInstance, itemId),
    cortexInstance.cortexGetZoomData(`${hrefJobs.item}=?zoom=`, 'code'),
    Items.findProductAssets(cortexInstance, hrefJobs.item)
  ).done(
    (avail, itemDetails, price, total, itemSelection, itemCode, assetUrl) => {

      const availability = cortexInstance.convertToObj(avail);
      const details = cortexInstance.convertToObj(itemDetails);
      const priceObj = cortexInstance.convertToObj(price);
      const theTotal = cortexInstance.convertToObj(total);
      const selection = cortexInstance.convertToObj(itemSelection);
      const code = cortexInstance.convertToObj(itemCode);
      if (assetUrl == null) {
        console.log('found a null asset');
        // console.log(itemCode);
        assetUrl = Items.constructProductAssetUrl(code._code[0].code);
      }
      const cartDataJSON = createLineItemJSONFromCortex(avail, priceObj, theTotal, details, selection, code, assetUrl);
      dfd.resolve(cartDataJSON);
    }
  );
  return dfd.promise();
}

function extractJobsForItem(links) {
  const hrefJobs = {};
  for (let i = 0; i < links.length; i++) {
    const link = links[i];
    const relationship = link.rel;
    switch (relationship) {
      case 'availability':
        hrefJobs.availability = link.href;
        break;
      case 'item':
        hrefJobs.item = link.href;
        break;
      case 'price':
        hrefJobs.price = link.href;
        break;
      case 'total':
        hrefJobs.total = link.href;
        break;
      default:
        break;
    }
  }
  return hrefJobs;
}

function extractShoppingCartDataToJSON(cortexInstance, loShoppingCartItems, callback) {
  if (!loShoppingCartItems.hasOwnProperty('_defaultcart')) {
    callback({});
    return;
  }
  const itemRows = loShoppingCartItems._defaultcart[0]._lineitems[0]._element;
  const lineItemCount = itemRows.length;
  let queriesReturned = 0;
  const lineItemAcc = [];
  itemRows.forEach((itemRow) => {
    const links = itemRow.links;
    const quantity = itemRow.quantity;
    const hrefJobs = extractJobsForItem(links);
    $.when(executeAllCartJobs(cortexInstance, hrefJobs)).done(
      (singleLineItemData) => {
        const data = cortexInstance.convertToObj(singleLineItemData);
        data.quantity = quantity;
        lineItemAcc.push(singleLineItemData);

        queriesReturned += 1;
        if (queriesReturned === lineItemCount) {
          callback(lineItemAcc);
        }
      }
    );
  });
}

// TODO: Use zooms to grab all the information for a particular item instead of going through the links and making more calls.
// defaultcart:lineitems:element:item:code,defaultcart:lineitems:element:item:definition
function shoppingCartParent(cortexInstance) {
  const dfd = $.Deferred();

  $.when(cortexInstance.cortexGetShoppingItems()).done(
    (loShoppingCartItems) => {
      const callback = (data) => {
        const returnData = cortexInstance.convertToObj(data);
        dfd.resolve(returnData);
      }
      const data = cortexInstance.convertToObj(loShoppingCartItems);
      extractShoppingCartDataToJSON(cortexInstance, data, callback);
    }
  );

  return dfd.promise();
}

/**
 * [isCartEmpty description]
 * @return {Boolean} [description]
 */
function isCartEmpty(cortexInstance) {
  var dfd = $.Deferred();

  var successCallback = (data) => {
    if (data == 0) {
      dfd.resolve(true);
    } else {
      dfd.resolve(false);
    }
  }

  var failureCallback = (error) => {
    console.log(error);
  }

  $.when(getShoppingCartQuantity(cortexInstance)).done(successCallback).fail(failureCallback);

  return dfd.promise();
}

/**
 * Returns the amount of items within the current users cart
 * @param  {Cortex} cortexInstance  the cortex instance containing the user token
 * @param  {function} successCallback function called when grabbing cart quantity succeeds -- passed the total amount result
 * @param  {function} failureCallback function called when grabbing the cart quantity fails
 * @return {null}
 */
function getShoppingCartQuantity(cortexInstance) {
  const dfd = $.Deferred();

  var quantitySuccess = function quantitySuccess(shoppingCartData) {
    var data = cortexInstance.convertToObj(shoppingCartData);
    var totalQuantity = data._defaultcart[0]['total-quantity'];
    console.log('totalQuantity: ' + totalQuantity);
    dfd.resolve(totalQuantity);
  };
  var quantityFailure = function quantityFailure(error) {
    console.log('failure getting the quantity');
    console.log(error);
    dfd.reject(error);
  };
  cortexInstance.cortexShoppingCartQuantity(quantitySuccess, quantityFailure);

  return dfd.promise();
}
/**
 * gets the total cost of the current items inside the cart
 * @param  {Cortex}   cortexInstance cortex object containing user token
 * @return {[JQuery.promise]}
 */
function getShoppingCartTotal(cortexInstance) {
  const dfd = $.Deferred();

  var success = function success(returnData) {
    var data = cortexInstance.convertToObj(returnData);
    var total = data._defaultcart[0]._total[0].cost[0];
    var display = total.display;
    var returnObj = {
      total: total,
      display: display
    };
    dfd.resolve(returnObj);
  };

  // TODO: change the name of this function;
  cortexInstance.cortexShoppingCartLinePrice(success);

  return dfd.promise();
}

function getCartDiscount(cortexInstance) {
  var dfd = $.Deferred();
  var zoomUrl = cortexInstance.cortexBaseUrl + '/?zoom=';
  var zoom = 'defaultcart:discount';

  $.when(cortexInstance.cortexGetZoomData(zoomUrl, zoom)).done(function (discountData) {
    var data = cortexInstance.convertToObj(discountData);
    if (data.hasOwnProperty('_defaultcart')) {
      var discount = data._defaultcart[0]._discount[0].discount[0].display;
      dfd.resolve(discount);
    } else {
      dfd.resolve('$0.00');
    }
  });
  return dfd.promise();
}

function getCartPromotion(cortexInstance) {
  // This will be the zoom function...
  var dfd = $.Deferred();
  var zoomUrl = cortexInstance.cortexBaseUrl + '/?zoom=';
  var zoom = 'defaultcart:appliedpromotions:element';

  $.when(cortexInstance.cortexGetZoomData(zoomUrl, zoom)).done(function (promotionData) {
    console.log('getting the promotion data');
    // TODO: We need something that will return even when there are no promotions
    var data = cortexInstance.convertToObj(promotionData);
    if (data.hasOwnProperty('_defaultcart')) {
      var nameArray = [];
      var elements = data._defaultcart[0]._appliedpromotions[0]._element;
      console.log(elements);
      for (var i = 0; i < elements.length; i++) {
        var currentElement = elements[i];
        var name = currentElement.name;
        nameArray.push(name);
      }
      // --> output the name Array...
      dfd.resolve(nameArray);
    } else {
      dfd.resolve('no promotions');
    }
  });
  return dfd.promise();
}
/**
 * get public test function
 */
function getCouponCodeFromOrder(cortexInstance) {
  var dfd = $.Deferred();
  var zoomUrl = cortexInstance.cortexBaseUrl + '/?zoom=';
  var zoom = 'defaultcart:order:couponinfo:coupon';
  $.when(cortexInstance.cortexGetZoomData(zoomUrl, zoom)).done(function (discountData) {
    console.log('return from the coupon code');
    var data = cortexInstance.convertToObj(discountData);
    if (data.hasOwnProperty('_defaultcart')) {
      var code = data._defaultcart[0]._order[0]._couponinfo[0]._coupon[0].code;
      dfd.resolve(code);
    } else {
      dfd.resolve(' no coupons');
    }
  });
  return dfd.promise();
}

/**
 * fetches any applied discounts on the current cart either through promotions, discounts, or coupons
 * @param  {Cortex} cortexInstance the cortex object containing the token
 * @return {null}
 */
function getPromotionDiscountForCart(cortexInstance) {
  var dfd = $.Deferred();
  $.when(
    getCartPromotion(cortexInstance),
    getCartDiscount(cortexInstance),
    getCouponCodeFromOrder(cortexInstance)
  )
  .done(function (promotion, discount, couponCode) {
    var promotionDiscount = {
      promotion: promotion,
      discount: discount,
      couponCode: couponCode
    };
    console.log(couponCode);
    dfd.resolve(promotionDiscount);
  });
  return dfd.promise();
}

/**
 * applies the promotional code to the current cart
 * @param  {String} cortexInstance - the cortex instance
 * @param  {String} postCode       - the promo code
 * @return {JQuery.promise}
 */
function applyPromoCode(cortexInstance, promoCode) {
  const dfd = $.Deferred();

  var baseUrl = cortexInstance.cortexBaseUrl;
  var zoom = 'defaultcart:order:couponinfo:couponform';
  $.when(cortexInstance.cortexGetZoomData(baseUrl + '?zoom=', zoom + '&_=')).done( (returnData) => {
    const data = cortexInstance.convertToObj(returnData);
    var href = data._defaultcart[0]._order[0]._couponinfo[0]._couponform[0].links[0].href;
    const success = (data) => {
      const dataObj = cortexInstance.convertToObj(data);
      dfd.resolve(dataObj);
    }
    const error = (err) => {
      dfd.reject(err);
    }
    cortexInstance.cortexPost(href, {
      code: promoCode
    }, success, error);
  });

  return dfd.promise();
}

function addToCart(cortexInstance, sku, quantity) {
  const dfd = $.Deferred();

  $.when(cortexInstance.cortexAddToCart(sku, quantity)).done( (cortexAddToCartData) => {
    const data = cortexInstance.convertToObj(cortexAddToCartData);
    let addToCartReturnData = {};
    addToCartReturnData.sku = sku;
    addToCartReturnData.currentItemQuantity = data.quantity;
    dfd.resolve(addToCartReturnData);
  });

  return dfd.promise();
}

module.exports.shoppingCartParent = shoppingCartParent;
module.exports.getShoppingCartQuantity = getShoppingCartQuantity;
module.exports.getShoppingCartTotal = getShoppingCartTotal;
module.exports.getPromotionDiscountForCart = getPromotionDiscountForCart;
module.exports.applyPromoCode = applyPromoCode;
module.exports.isCartEmpty = isCartEmpty;
module.exports.addToCart = addToCart;
