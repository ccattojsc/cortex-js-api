let $ = null;

try {
  // TODO: Only bring in JSDOM and NAJAX if the user says they are going to use the libs server side.
  const najax = require('najax');
  const jsdom = require('jsdom').jsdom;
  const doc = jsdom();
  const window = doc.defaultView;
  $ = require('jquery')(window);
  $.ajax = najax;
} catch (error) {
  $ = require('jquery');
}

const Cortex = require('./../Cortex.js');
const Utilities = require('./../Utilities.js');


function extractAddressFromUserAccount(cortexInstance) {
  const dfd = $.Deferred();
  const zoomUrl = `${cortexInstance.cortexBaseUrl}/?zoom=`;
  const zoom = 'defaultprofile:addresses:element';

  $.when(cortexInstance.cortexGetZoomData(zoomUrl, zoom)).done(
    (zoomData) => {
      const data = cortexInstance.convertToObj(zoomData);
      const address = data._defaultprofile[0]._addresses[0]._element[0].address;
      dfd.resolve(address);
    });

  return dfd.promise();
}

/**
 * gets all account information from dynamo db and cortex.
 * @param  {Cortex} cortexInstance - the Cortex Instance
 * @return {Object} - Returns all account information in JSON structure
 */
function getAllAccountInformation(cortexInstance) {
  const dfd = $.Deferred();
  const zoomUrl = `${cortexInstance.cortexBaseUrl}/?zoom=`;
  const zoom = 'defaultprofile';
  $.when(
    extractAddressFromUserAccount(cortexInstance)
  ).done(
    (addressData) => {
      const returnObj = {
        addressData
      };
      dfd.resolve(returnObj);
    }
  );

  return  dfd.promise();
}

/**
 * [addUserAttributesToAccount Adds an address and payment information to cortex account]
 * @param {Cortex} cortexInstance - the Cortex Instance
 * @param {JSON} payload        - The json structure that will carry address and payment information
 */
function addUserAttributesToAccount(cortexInstance, payload) {
  console.log('adding the attributes to account');
  console.log('printing the payload...');
  console.log(payload);
  console.log(payload.lastName);
  console.log(payload.firstName);
  const dfd = $.Deferred();
  const address = payload.address;
  $.when(
    // This will create the address
    cortexInstance.cortexCreateAddress(
      address.countryName,
      address.extendedAddress,
      address.locality,
      address.postalCode,
      address.region,
      address.streetAddress,
      payload.lastName,
      payload.firstName
    ),
    // this will create the payment method..
    cortexInstance.cortexCreatePaymentMethod(
      payload.payment.displayName,
      payload.payment.paymentToken
    )
  ).done(
    () => {
      console.log('successfully created address and payment');
      dfd.resolve();
    }
  );
  return dfd.promise();
}

/**
 * registers user
 * @param  {Object} payload - All the information needed to register a user
 * @param  {String} baseUrl - cortex base url
 * @param  {String} scope   - store scope
 * @return {Object} null
 */
function registerUser(payload, baseUrl, scope) {
  const dfd = $.Deferred();
  $.when(Utilities.createAnonCortexInstance(baseUrl, scope)).done(
    (cortexInstance) => {
      const errorOnRegister = (errorWhileRegistering) => {
        dfd.reject(errorWhileRegistering);
      };
      $.when(cortexInstance.cortexRegister(
        payload.email,
        payload.password,
        payload.firstName,
        payload.lastName,
        errorOnRegister)).done(
        () => {
          $.when(Utilities.createCortexInstance(payload.email, payload.password, baseUrl, scope)).done(
            // TODO: need to fix this here.
            (newlyRegisteredCortexInstance) => {
              if (payload.payment != null && payload.address != null) {
                $.when(addUserAttributesToAccount(newlyRegisteredCortexInstance, payload)).done(
                  () => {
                    console.log('successfully added all account details..');
                    dfd.resolve(newlyRegisteredCortexInstance);
                  }
                );
              } else {
                dfd.resolve(newlyRegisteredCortexInstance);
              }
            }
          );
        }
      )
    }
  );
  return dfd.promise();
}

/**
 * Returns the default payment token from a users profile.
 * @param {Cortex} cortexInstance - the Cortex Instance
 * @param  {String} callback - The callback function on success, takes the existing payment token.
 * @return {Object} null
 */
function getPaymentToken(cortexInstance) {
  const dfd = $.Deferred();

  var zoomUrl = cortexInstance.cortexBaseUrl + '/?zoom='
  var zoom = 'defaultprofile:paymentmethods:element';

  $.when(cortexInstance.cortexGetZoomData(zoomUrl, zoom)).done(
    function(returnData) {
      let data = cortexInstance.convertToObj(returnData);
      var token = data._defaultprofile[0]._paymentmethods[0]._element[0].token;
      dfd.resolve(token);
    });

  return dfd.promise();
}

/**
 * Returns the current user's default billing address
 * @param  {[Cortex]} cortexInstance [The Cortex Instance in use]
 * @return {JQuery.promise}
 */
function getDefaultBillingAddress(cortexInstance) {
  const dfd = $.Deferred();

  var zoomUrl = cortexInstance.cortexBaseUrl + '/?zoom=';
  var zoom = 'defaultprofile:addresses:billingaddresses:default';
  $.when(cortexInstance.cortexGetZoomData(zoomUrl, zoom)).done(function (returnData) {
    const data = cortexInstance.convertToObj(returnData);
    const billingAddress = data._defaultprofile[0]._addresses[0]._billingaddresses[0]._default[0];
    dfd.resolve(billingAddress);
  });

  return dfd.promise()
}

/**
 * Returns the current user's default shipping address
 * @param  {[Cortex]} cortexInstance [The Cortex Instance in use]
 * @return {JQuery.promise}
 */
function getDefaultShippingAddress(cortexInstance) {
  const dfd = $.Deferred();

  var zoomUrl = cortexInstance.cortexBaseUrl + '/?zoom=';
  var zoom = 'defaultprofile:addresses:shippingaddresses:default';
  $.when(cortexInstance.cortexGetZoomData(zoomUrl, zoom)).done(function (returnData) {
    const data = cortexInstance.convertToObj(returnData);
    const shippingAddress = data._defaultprofile[0]._addresses[0]._shippingaddresses[0]._default[0]
    dfd.resolve(shippingAddress);
  });

  return dfd.promise();
}

module.exports.getAllAccountInformation = getAllAccountInformation;
module.exports.registerUser = registerUser;
module.exports.addUserAttributesToAccount = addUserAttributesToAccount;
module.exports.getPaymentToken = getPaymentToken;
module.exports.getDefaultShippingAddress = getDefaultShippingAddress;
module.exports.getDefaultBillingAddress = getDefaultBillingAddress;
