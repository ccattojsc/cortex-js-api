// ***************************************************************
// Copyright © 2018 Elastic Path Software Inc. All rights reserved.
// ***************************************************************
let $ = null;

try {
  // TODO: Only bring in JSDOM and NAJAX if the user says they are going to use the libs server side.
  const najax = require('najax');
  const jsdom = require('jsdom').jsdom;
  const doc = jsdom();
  const window = doc.defaultView;
  $ = require('jquery')(window);
  $.ajax = najax;
} catch (error) {
  $ = require('jquery');
}

const Settings = require('./../Config/settings.js').settings;
const Utilities = require('./../Utilities.js');

const errorHandler = (error) => {
  console.log('error');
  console.log(error);
};

function extractChildrenElements(elements) {
  const links = elements.links;
  const childArray = [];
  for (let i = 0; i < links.length; i++) {
    const currentLink = links[i];
    if (currentLink.rel === 'child') {
      childArray.push(currentLink);
    }
  }
  return childArray;
}
function findItemsHref(links) {
  let itemHref = null;
  links.forEach((link) => {
    if (link.rel === 'items') {
      itemHref = link.href;
    }
  });
  return itemHref;
}
function findCategoryItemCode(href) {
  const splitLink = href.split('/');
  const categoryCodeWithEqual = splitLink[splitLink.length - 1];
  const categoryCode = categoryCodeWithEqual.split('=')[0];
  return categoryCode;
}

function extractItemCategoryParameters(element) {
  const categoryName = element['display-name'];
  const categoryDetails = element.details;
  const itemHref = findItemsHref(element.links);
  const categoryCode = findCategoryItemCode(itemHref);
  return {
    categoryName,
    categoryDetails,
    itemHref,
    categoryCode
  };
}

function findProductDefinition(cortexInstance, href) {
  const dfd = $.Deferred();
  $.when(cortexInstance.cortexGetZoomData(href, '=?zoom=definition')).done(
    (data) => {
      data = cortexInstance.convertToObj(data);
      const definition = data._definition[0];
      const obj = {
        displayName: definition['display-name'],
        details: definition.details
      };
      dfd.resolve(obj);
    }
  ).fail(
    (error) => {
      console.log('unable to find a definition');
      dfd.reject(error);
    }
  );

  return dfd.promise();
}

function findProductAvailability(cortexInstance, href) {
  // TODO: Implement the product availibility...
  const dfd = $.Deferred();
  $.when(cortexInstance.cortexGetZoomData(href, '=?zoom=availability')).done(
    (data) => {
      data = cortexInstance.convertToObj(data);
      let availabilityState;
      if (data.hasOwnProperty('_availability')) {
        availabilityState = findAvailibilityFromArray(data);
      }
      dfd.resolve(availabilityState);
    }
  ).fail(
    (error) => {
      console.log('unable to find a definition');
      dfd.reject(error);
    }
  );
  return dfd.promise();
}

function findAvailibilityFromArray(data) {
  const availabilityArray = data['_availability'];

  for (let i = 0; i < availabilityArray.length; i++) {
    if (availabilityArray[i].state !== undefined) {
      return availabilityArray[i].state;
    }
  }
}

function findStandaloneItemFromLinks(links) {
  for (let i = 0; i < links.length; i++) {
    const currentLink = links[i];
    if (currentLink.rel === 'standaloneitem') {
      return currentLink;
    }
  }
  return null;
}

// TODO: We need to fetch the image from here as well...
// We need a promise for this call here...
function createBundleArray(cortexInstance, bundles) {
  const dfd = $.Deferred();
  const bundleArray = [];
  let promisesReturned = 0;
  bundles.forEach((bundle) => {
    const quantity = bundle.quantity;
    const displayName = bundle['display-name'];
    const details = bundle.details;

    const standaloneItemHref = findStandaloneItemFromLinks(bundle.links);

    // There are no assets for the gallo set for some reason...
    $.when(findProductAssets(cortexInstance, standaloneItemHref)).done(
      (assetUrl) => {
        promisesReturned ++;
        const elementObj = {
          quantity,
          displayName,
          details,
          assetUrl
        };
        bundleArray.push(elementObj);
        if (bundles.length === promisesReturned) {
          dfd.resolve(bundleArray);
        }
      }
    ).fail(
      (error) => {
        console.log('unable to find product assets in bundle');
        dfd.reject(error);
      }
    );
  });
  return dfd.promise();
}

function findProductBundles(cortexInstance, href) {
  const dfd = $.Deferred();
  $.when(cortexInstance.cortexGetZoomData(href, '=?zoom=definition:components:element')).done(
    (bundleData) => {
      const data = cortexInstance.convertToObj(bundleData);
      if (data.hasOwnProperty('_definition')) {
        const bundles = data._definition[0]._components[0]._element;
        // TODO: when creating the bundle we need to grab the relative path to the image as well...
        $.when(createBundleArray(cortexInstance, bundles)).done(
          (bundle) => {
            dfd.resolve(bundle);
          }
        );
      } else {
        // bundle doesn't exist for item
        dfd.resolve(null);
      }
    }
  ).fail(
    (error) => {
      console.log('query for bundle failed');
      dfd.reject(error);
    }
  );

  return dfd.promise();
}

function constructAssetUrl(relativeLocation) {
  relativeLocation = relativeLocation.split('/');
  relativeLocation = relativeLocation[relativeLocation.length - 1];
  const spacesReplaced = relativeLocation.replaceAll(' ', '%20');
  const assetsLink = "null for now";
  return assetsLink;
}
/**
 * given a cortex product link, the function will grab the asset url
 * @param  {Cortex} cortexInstance cortex object containing token
 * @param  {String} link           item link: ex. http://54.201.115.161:9080/cortex/items/mobee/qgqvhmluoq4tmnrqgaywc5s7onsf6ytvpe=
 * @return {jQuery.Deferred.Promise} Returns the promise, when resolved the new Url is passed through
 */
function findProductAssets(cortexInstance, link) {
  const dfd = $.Deferred();
  let url;
  if (link.hasOwnProperty('href')) {
    url = link.href;
  } else {
    url = link;
  }
  $.when(cortexInstance.cortexGetZoomData(url, '=?zoom=definition:assets:element'))
  .done(
    (assetData) => {
      const data = cortexInstance.convertToObj(assetData);
      if (data.hasOwnProperty('_definition')) {
        const definition = data._definition[0];
        if (definition.hasOwnProperty('_assets')) {
          const relativeLocation = definition._assets[0]._element[0]['relative-location'];
          const newUrl = constructAssetUrl(relativeLocation, cortexInstance);
          dfd.resolve(newUrl);
          return;
        }
      }
      dfd.resolve(null);
    }
  )
  .fail(
    (error) => {
      console.log('unable to find a product asset');
      dfd.reject(error);
    }
  );
  return dfd.promise();
}

function findProductPrice(cortexInstance, href) {
  const dfd = $.Deferred();

  // BUG: What happens when price resource does not exist for certain items...
  // Have to add a timeout to it..
  $.when(cortexInstance.cortexGetZoomData(href, '?zoom=price')).done(
    (priceZoomData) => {
      const data = cortexInstance.convertToObj(priceZoomData);
      if (data.hasOwnProperty('_price')) {
        const purchasePrice = data._price[0]['purchase-price'];
        const listPrice = data._price[0]['list-price'];
        const returnPriceObj = {
          listPrice,
          purchasePrice
        };
        dfd.resolve(returnPriceObj);
      } else {
        dfd.resolve({});
      }
    }
  ).fail(
    (error) => {
      console.log(error);
      console.log('unable to find a price');
      dfd.reject(error);  // change this line to resolve instead of reject...
    }
  );
  return dfd.promise();
}

function findProductCode(cortexInstance, href) {
  const dfd = $.Deferred();
  $.when(cortexInstance.cortexGetZoomData(href, '=?zoom=code')).done(
    (skuCode) => {
      skuCode = cortexInstance.convertToObj(skuCode);
      const sku = skuCode._code[0].code;
      dfd.resolve(sku);
    }
  ).fail(
    (error) => {
      console.log('unable to find product code');
      dfd.reject(error);
    }
  );
  return dfd.promise();
}

function createItemObj(cortexInstance, definition, code, price, assets, availability, bundles, categoryName) {
  const returnDefinition = cortexInstance.convertToObj(definition);
  returnDefinition.sku = code;
  returnDefinition.price = price;
  returnDefinition.assetUrl = assets;
  returnDefinition.bundle = bundles;
  returnDefinition.category = categoryName;
  returnDefinition.availability = availability;
  return returnDefinition;
}

function constructProductAssetUrl(code) {
  const urlPrefix = Settings.S3AssetUrlPrefix;
  return urlPrefix + code + '.png';
}

function constructCategoryAssetUrl(categoryName) {
  const urlPrefix = Settings.S3AssetUrlPrefix;
  categoryName = categoryName.replace(/\s/g, '');
  return urlPrefix + categoryName + '.png';
}

// HAX: -- Currently this is a duplicate -- We need the recommended products to subclass this class...
/**
 * function is used to grab all attributes pertaining to a particular Cortex item.
 * @param  Cortex cortexInstance
 * @param  [] links - list of items where the item link is located in array.href
 * @return [Object] - returns an array of objects, each object containing item specific data
 */
function createItemList(cortexInstance, links, categoryName) {
  const dfd = $.Deferred();
  const linkLength = Utilities.findNumberOfElements(links);
  // lets just extract the number of elements instead
  let returnCount = 0;
  let itemAcc = [];
  links.forEach((link) => {
    // TODO: Refactor to get the link of hrefs first... and then use $.when.apply($, [requests]);
    if (link.rel === 'element') {
      // We know for sure that it is the price that is causing it to fail...
      $.when(
          findProductDefinition(cortexInstance, link.href),
          findProductCode(cortexInstance, link.href),
          findProductPrice(cortexInstance, link.href),
          findProductAssets(cortexInstance, link.href),
          findProductBundles(cortexInstance, link.href),
          findProductAvailability(cortexInstance, link.href)
        ).done(
          (definition, code, price, assets, bundles, availability) => {
            if (assets == null) {
                assets = constructProductAssetUrl(code);
            }

            const newItem = createItemObj(cortexInstance, definition, code, price, assets, availability, bundles, categoryName);
            itemAcc.push(newItem);

            returnCount += 1;
            if (returnCount === linkLength) {
              dfd.resolve(itemAcc);
            }
          }
        ).fail(
          (error) => {
            console.log('failing to create the item here');
            returnCount += 1;
            if (returnCount === linkLength) {
              dfd.resolve(itemAcc);
            }
          });
    }
    if (link.rel === 'next') {
      $.when(createItemListHelper(cortexInstance, link, categoryName)).done(
        (nestedItems) => {
          returnCount += 1;
          if (returnCount === linkLength) {
            itemAcc = itemAcc.concat(nestedItems);
            dfd.resolve(itemAcc);
          }
        }
      );
    }
  });
  return dfd.promise();
}

function createItemListHelper(cortexInstance, link, name) {
  const dfd = $.Deferred();

  const onSuccess = (nextPageData) => {
    const data = cortexInstance.convertToObj(nextPageData);
    const links = data.links;
    $.when(createItemList(cortexInstance, links, name)).done(
      (items) => {
        dfd.resolve(items);
      }
    );
  };

  cortexInstance.cortexGet(link.href, onSuccess, errorHandler);

  return dfd.promise();
}

function doesCategoryContainSubCategory(element) {
  const links = element.links;
  for (let i = 0; i < links.length; i++) {
    const currentLink = links[i];
    if (currentLink.rel === 'child') {
      return true;
    }
  }
  return false;
}

function getItemsInCategory(cortexInstance, name, categoryCode) {
  const dfd = $.Deferred();
  $.when(cortexInstance.cortexGetCategoryItemLinks(categoryCode)).done(
    (categoryItemsData) => {
      const data = cortexInstance.convertToObj(categoryItemsData);
      const links = data._items[0].links; // HAX --> we need to go through the items here...
      if (links.length !== 0) {
        $.when(createItemList(cortexInstance, links, name)).done(
            (listOfItemsInCategory) => {
              dfd.resolve(listOfItemsInCategory);
            }
          ).fail(
            (error) => {
              console.error('there was an error');
            }
          );
      } else {
        dfd.resolve([]);
      }
    }
  ).fail(
    () => {
      console.warn('get items in category failing');
      dfd.resolve([]);
    }
  );
  return dfd.promise();
}
function getItemsFromChild(cortexInstance, params) {
  const dfd = $.Deferred();
  const childAcc = {};
  $.when(getItemsInCategory(cortexInstance, params.categoryName, params.categoryCode)).done(
    (itemsInCategory) => {
      childAcc[params.categoryName] = itemsInCategory;
      dfd.resolve(childAcc);
    }
  );
  return dfd.promise();
}

function caseInsensitiveStringCompare(str1, str2) {
  let str1Insensitized;
  let str2Insensitized;
  if (str1 != null) {
    str1Insensitized = str1.toUpperCase().replace(/[^\w\s]|_/g, '').replace(/\s+/g, '');
  } else {
    return true;
  }
  if (str2 != null) {
    str2Insensitized = str2.toUpperCase().replace(/[^\w\s]|_/g, '').replace(/\s+/g, '');
  } else {
    return true;
  }
  if (str1Insensitized === str2Insensitized) {
    return true;
  }
  return false;
}

function shouldGatherCurrentCategoryItems(categoryName1, categoryName2) {
  if (caseInsensitiveStringCompare(categoryName1, categoryName2)) {
    return true;
  }
  return false;
}

function isThereChosenCategoryAtCurrentDepth(categoryArray, categoryDepth) {
  if (categoryArray.length > categoryDepth) {
    return true;
  }
  return false;
}

function getExpectedPromiseReturnCount(categoryArray, categoryDepth, elements) {
  if (isThereChosenCategoryAtCurrentDepth(categoryArray, categoryDepth)) {
    return 1;
  }
  return elements.length;
}

/**
 * when called will provide the price of the object... NOTE this function is still WIP.  Should return all properties of product
 * @param  {Cortex} cortexInstance Cortex object containing token
 * @param  {String} sku            The sku of a particular product
 * @return {JQuery.Deferred.Promise} Returns promise, when resolved provides pricing for object
 */
function getItem(cortexInstance, sku) {
  const dfd = $.Deferred();

  $.when(cortexInstance.cortexGetItem(sku, null)).done((itemData) => {
    const data = cortexInstance.convertToObj(itemData);
    const href = data.self.href;
    $.when(
      findProductDefinition(cortexInstance, href),
      findProductCode(cortexInstance, href),
      findProductPrice(cortexInstance, href),
      findProductAssets(cortexInstance, href),
      findProductBundles(cortexInstance, href),
      findProductAvailability(cortexInstance, href)
    ).done(
      (definition, code, price, assets, bundles, availability) => {
        const newItem = createItemObj(cortexInstance, definition, code, price, assets, availability, bundles, null);
        dfd.resolve(newItem);
      });
  }).fail((err)=>{
    dfd.reject(err);
  });

  return dfd.promise();
}

// TODO: make this function recursively get children...
function getAllItemsFromChildren(cortexInstance, itemAcc, element, categoryArray, categoryDepth) {
  const dfd = $.Deferred();
  const childrenElements = extractChildrenElements(element);

  const childrenLength = getExpectedPromiseReturnCount(categoryArray, categoryDepth, childrenElements);
  let childPromisesReturned = 0;

  childrenElements.forEach((child) => {
    const success = (childContents) => {
      const contents = cortexInstance.convertToObj(childContents);
      const params = extractItemCategoryParameters(contents);
      const chosenCategory = categoryArray[categoryDepth];
      if (!shouldGatherCurrentCategoryItems(chosenCategory, params.categoryName)) {
        return;
      }
      $.when(getItemsFromChild(cortexInstance, params)).done(
        (childItems) => {
          childPromisesReturned++;

          const itemObj = {
            items: childItems[params.categoryName],
            categoryName: params.categoryName,
            nestedCategories: []
          };

          itemAcc.push(itemObj);

          if (doesCategoryContainSubCategory(contents)) {
            $.when(
              getAllItemsFromChildren(
                cortexInstance,
                itemObj.nestedCategories,
                contents,
                categoryArray,
                categoryDepth += 1
              )).done(
              (childAcc) => {
                if (childrenLength === childPromisesReturned) {
                  dfd.resolve(childAcc);
                }
              }
            );
          } else if (childrenLength === childPromisesReturned) {
            dfd.resolve(itemAcc);
          }
        }
      );
    };
    cortexInstance.cortexGet(child.href, success, errorHandler);
  });
  return dfd.promise();
}

function getAllItemsHelper(cortexInstance, itemAcc, categoryArray, categoryDepth, initialCategoryData, dfd) {
  const elements = initialCategoryData._element;
  let promiseReturnCount = 0;

  const elementLength = getExpectedPromiseReturnCount(categoryArray, categoryDepth, elements);

  elements.forEach((element) => {
    const params = extractItemCategoryParameters(element);

    const chosenCategory = categoryArray[categoryDepth];

    if (!shouldGatherCurrentCategoryItems(chosenCategory, params.categoryName)) {
      return;
    }

    $.when(getItemsInCategory(cortexInstance, params.categoryName, params.categoryCode)).done(
      (itemsInCategory) => {

        const item = {
          items: itemsInCategory,
          categoryName: params.categoryName,
          nestedCategories:[]
        }

        if (doesCategoryContainSubCategory(element)) {
          $.when(
            getAllItemsFromChildren(
              cortexInstance,
              item.nestedCategories,
              element,
              categoryArray,
              categoryDepth += 1)).done(
            (nestedItems) => {
              const nestedCategory = {
                items: itemsInCategory,
                categoryName: params.categoryName,
                nestedCategories: nestedItems
              }
              itemAcc.Categories.push(nestedCategory);
              promiseReturnCount += 1;
              if (promiseReturnCount === elementLength) {
                dfd.resolve(itemAcc);
              }
            }
          );
        } else {
          itemAcc.Categories.push(item);
          promiseReturnCount += 1;
          if (promiseReturnCount === elementLength) {
            dfd.resolve(itemAcc);
          }
        }
      }
    );
  });
}

function turnCategoryPathIntoArray(categoryPath) {
  if (categoryPath != null) {
    const categoryArray = categoryPath.split('/');
    return categoryArray;
  }
  return [];
}

/**
  * getAllItems in root or specified category
  * @param {Cortex} cortexInstance cortex object instance with token
  * @param {String} categoryPath leave this undefined or null for all items starting from the root. To find items in a specific category then provide a path. EX. Mens/Shirts
  * @return {jQuery.Deferred.Promise} returns a promise, when resolved will provide all items in categorical structure
*/
function getAllItems(cortexInstance, categoryPath) {
  const dfd = $.Deferred();
  const categoryArray = turnCategoryPathIntoArray(categoryPath);

  $.when(cortexInstance.cortexGetListOfCategories()).done(
    (categoryData) => {
      const data = cortexInstance.convertToObj(categoryData);
      getAllItemsHelper(cortexInstance, {Categories: []}, categoryArray, 0, data, dfd);
    }
  );
  return dfd.promise();
}

function getItemCategoriesChildren(cortexInstance, itemAcc, element, categoryArray, categoryDepth, dfd) {
  const childrenElements = extractChildrenElements(element);

  if (childrenElements.length === 0) {
    dfd.resolve('no categories with that path');
  }

  const childrenLength = getExpectedPromiseReturnCount(categoryArray, categoryDepth, childrenElements);
  let childPromisesReturned = 0;

  const returnCategories = [];

  const chosenCategory = categoryArray[categoryDepth];

  childrenElements.forEach((child) => {
    const success = (categories) => {
      childPromisesReturned++;
      const data = cortexInstance.convertToObj(categories);
      const params = extractItemCategoryParameters(data);

      if (chosenCategory == null) {
        returnCategories.push(params);
        if (childPromisesReturned === childrenLength) {
          dfd.resolve(returnCategories);
        }
      } else if (shouldGatherCurrentCategoryItems(chosenCategory, params.categoryName)) {
        getItemCategoriesChildren(cortexInstance, itemAcc, data, categoryArray, categoryDepth + 1, dfd);
      }
    };
    cortexInstance.cortexGet(child.href, success, errorHandler);
  });
}

function getItemCategoriesHelper(cortexInstance, categoryAcc, categoryArray, categoryDepth, initialCategoryData, dfd) {
  const elements = initialCategoryData._element;
  const promiseReturnCount = 0;

  const elementLength = getExpectedPromiseReturnCount(categoryArray, categoryDepth, elements);

  const returnCategories = [];
  const chosenCategory = categoryArray[categoryDepth];

  elements.forEach((element) => {
    const params = extractItemCategoryParameters(element);

    if (shouldGatherCurrentCategoryItems(chosenCategory, params.categoryName)) {
      getItemCategoriesChildren(cortexInstance, categoryAcc, element, categoryArray, categoryDepth + 1, dfd);
      return;
    }
    returnCategories.push(params);
  });

  if (chosenCategory == '') {
    dfd.resolve(returnCategories);
  }
}
/**
 * Provides all categories
 * @param  {[type]} cortexInstance cortex object containing token
 * @param  {[type]} categoryAcc    accumulator that will hold all the categories.  Ex. {}... TODO: refactor might not need to input this
 * @param  {[type]} categoryPath   The category which we would like to see more subcategories of.  Ex. /Mens/Shirts or Ex. '/' for the entire category tree
 * @param  {[type]} categoryDepth  [description]
 * @return {[type]}                [description]
 */
function getItemCategories(cortexInstance, categoryPath) {
  // TODO: this function will only output the categories...
  const dfd = $.Deferred();
  const categoryArray = turnCategoryPathIntoArray(categoryPath);

  $.when(cortexInstance.cortexGetListOfCategories()).done(
    (categoryData) => {
      const data = cortexInstance.convertToObj(categoryData);
      getItemCategoriesHelper(cortexInstance, {}, categoryArray, 0, data, dfd);
    }
  );
  return dfd.promise();
}

/**
 * getItemChoices -- Finds other variants of the same product and returns the selection link
 * @param {Cortex} cortexInstance - Cortex Object
 * @param {String} sku - the sku code we would like to find variants for.
 */
 function getItemChoices(cortexInstance, sku) {
   var dfd = $.Deferred();
   var getInstanceId = function getInstanceId(uri) {
     var instanceIdTemp = uri.split('/');
     return instanceIdTemp[3].split('=')[0];
   };
   var cortexGetItemCallback = function cortexGetItemCallback(itemCallbackData) {
     var data = cortexInstance.convertToObj(itemCallbackData);
     var instanceId = getInstanceId(data.self.uri);
     $.when(getItemChoicesHelper(cortexInstance, instanceId)).done(
       (data) => {
         dfd.resolve(data);
       }
     );
   };
   cortexInstance.cortexGetItem(sku, 'price', cortexGetItemCallback);
   return dfd.promise();
 }

 function getItemChoicesHelper(cortexInstance, itemCode) {
   var dfd = $.Deferred();
   var cortexSelectionCallback = function cortexSelectionCallback(data) {
     data = cortexInstance.convertToObj(data);
     var elements = data._element;

     var elementReturnCount = countNestedElementLinks(elements);

     var returnCount = 0;

     var returnObj = [];

     elements.forEach(function (element) {
       var links = element._selector[0].links;

       var linkReturnSize = links.length;
       var linkReturnCount = 0;

       var choices = [];

       links.forEach(function (link) {
         var href = link.href;
         var status = link.rel;

         if (status === 'choice' || status === 'chosen') {
           $.when(createChoiceObj(cortexInstance, href)).done(
           // TODO: aChan - need to refactor this... repetitive code.
           function (choiceObj) {
             choices.push(choiceObj);

             linkReturnCount += 1;
             if (linkReturnCount == linkReturnSize) {
               returnObj.push(choices);
             }

             returnCount += 1;
             if (returnCount == elementReturnCount) {
               dfd.resolve(returnObj);
             }
           });
         }
         if (status === 'option') {
           $.when(getChoiceCategory(cortexInstance, href)).done(function (choiceCategoryObj) {
             choices.push(choiceCategoryObj);

             linkReturnCount += 1;
             if (linkReturnCount === linkReturnSize) {
               returnObj.push(choices);
             }

             returnCount += 1;
             if (returnCount === elementReturnCount) {
               dfd.resolve(returnObj);
             }
           });
         }
       });
     });
   };
   cortexInstance.cortexShowItemSelection(itemCode, cortexSelectionCallback);
   return dfd.promise();
 }

 function getChoiceCategory(cortexInstance, href) {
   var dfd = $.Deferred();
   var success = function success(data) {
     // data = cortexInstance.convertToObj(data);
     var obj = {
       displayName: data['display-name'],
       name: data.name
     };
     dfd.resolve(obj);
   };
   var errorHandler = function errorHandler(error) {
     dfd.resolve(error);
   };
   cortexInstance.cortexGet(href, success, errorHandler);
   return dfd.promise();
 }

 function countNestedElementLinks(elements) {
   var counter = 0;
   elements.forEach(function (element) {
     var links = element._selector[0].links;
     var linkSize = links.length;
     counter += linkSize;
   });
   return counter;
 }

 function createChoiceObj(cortexInstance, href) {
   var dfd = $.Deferred();
   $.when(cortexInstance.cortexGetChoiceHrefs(href).done(function (data) {
     // data = cortexInstance.convertToObj(data);
     var dataHref = data.description;
     var selectActionHref = data.selectAction;
     $.when(getChoiceDescription(cortexInstance, dataHref)).done(function (name) {
       var choiceObj = {
         name: name,
         selectAction: selectActionHref
       };
       dfd.resolve(choiceObj);
     });
   }));
   return dfd.promise();
 }

 function getChoiceDescription(cortexInstance, url) {
   var dfd = $.Deferred();
   cortexInstance.cortexGet(url, function (data) {
     // data = cortexInstance.convertToObj(data);
     dfd.resolve(data['display-name']);
   }, function () {
     console.log('error retrieving choice description');
   });
   return dfd.promise();
 }

 /**
  * Will query keyword cortex resource
  * @param  {[Object]} cortexInstance - The cortex resource
  * @param  {[String]} keyword        - The keyword to be searched
  * @return {[JQuery.Deferred.Promise]} - Returns a jquery promise
  */
function getItemsByKeyword(cortexInstance, keyword) {
    var dfd = $.Deferred();
    const success = (res) => {
        var data = cortexInstance.convertToObj(res);

        var result = [];
        if (data._element && data._element.length > 0) {
            data._element.forEach((el) => {
                var newElement = {};
                newElement.code = el._code[0].code;
                newElement.details = el._definition[0].details;
                newElement.displayName = el._definition[0]['display-name'];
                if (el._price) {
                    newElement.listPrice = el._price[0]['list-price'];
                    newElement.purchasePrice = el._price[0]['purchase-price'];
                } else {
                    newElement.listPrice = {};
                    newElement.purchasePrice = {};
                }
                result.push(newElement);
            });
            dfd.resolve(result);
        } else {
            dfd.resolve({});
        }
    };

    const fail = (err) => {
        console.log('CORTEX::ITEMS::getItemsByKeyword()::ERROR::' + err);
        dfd.reject(err);
    };

    const url = cortexInstance.cortexBaseUrl + '/searches/' + cortexInstance.scope
        + '/keywords/items?followlocation&zoom=element:code,element:definition,element:price';
    cortexInstance.cortexPost(url, {keywords: keyword}, success, fail);

    return dfd.promise();
}

/**
 * Will add a particular item to the wishlist based on sku
 * @param  {[type]} cortexInstance - the cortex instance
 * @param {[type]} sku - the Item sku
 */
function addToWishlist(cortexInstance, sku) {
  const dfd = $.Deferred();

  const success = (data) => {
      data = cortexInstance.convertToObj(data);
      if (data.hasOwnProperty('_addtowishlistform')) {
        const wishlistActionLink = data['_addtowishlistform'][0].links[0].href;
        const success = (data) => {
          dfd.resolve(data);
        }
        const fail = (error) => {
          dfd.reject(error);
        }
        cortexInstance.cortexPost(wishlistActionLink, {}, success, fail);
      }
  }
  cortexInstance.cortexGetItem(sku, 'addtowishlistform', success)

  return dfd.promise();
}

// TODO: Need to include error handling in this function.
/**
 * Will get the list of wishlist items
 * @param  {[type]} cortexInstance - the cortex instance
 */
function getWishlistItems(cortexInstance) {
  const dfd = $.Deferred();

  $.when(cortexInstance.cortexGetWishlist()).done(
    (wishlistZoomData) => {
      const data = cortexInstance.convertToObj(wishlistZoomData);

      if (!data.hasOwnProperty('_defaultwishlist')) {
        dfd.resolve({});
      }

      const lineItems = data['_defaultwishlist'][0]['_lineitems'][0]['_element'];
      let returnedPromises = 0;
      let wishlistItem = {};

      lineItems.forEach(
        (lineItem) => {
          const links = lineItem.links;
          const selfHref = lineItem.self.href;
          const currentItemUriHash = findInstanceIdFromHref(selfHref);

          links.forEach(
            (link)=> {

              if (link.rel == 'item') {
                const itemLink = link.href;
                $.when(
                  findProductDefinition(cortexInstance, link.href),
                  findProductCode(cortexInstance, link.href),
                  findProductPrice(cortexInstance, link.href),
                  findProductAssets(cortexInstance, link.href),
                  findProductBundles(cortexInstance, link.href),
                  findProductAvailability(cortexInstance, link.href)
                ).done(
                  (definition, code, price, assets, bundles, availability) => {
                    const newItem = createItemObj(cortexInstance, definition, code, price, assets, availability, bundles, null);

                    if (wishlistItem[currentItemUriHash] == undefined) {
                      wishlistItem[currentItemUriHash] = {
                        wishlistItem: newItem
                      }
                    } else {
                      wishlistItem[currentItemUriHash].wishlistItem = newItem;
                    }

                    returnedPromises++;
                    if (returnedPromises == lineItems.length) {
                      dfd.resolve(wishlistItem);
                    }
                  }
                );
              }

              if (link.rel == 'movetocartform') {
                if (wishlistItem[currentItemUriHash] == undefined) {
                  wishlistItem[currentItemUriHash] = {
                    movetocartform: link.href
                  }
                } else {
                  wishlistItem[currentItemUriHash].movetocartform = link.href;
                }
              }
          });
        }
      );
    });
  return dfd.promise();
}

function findInstanceIdFromHref(href) {
  const splitHref = href.split("/");
  lastElement = splitHref[splitHref.length - 1];
  lastElement = lastElement.replace(/=([^;]*)/g, '');
  return lastElement;
}
// ---> GetItemChoices
module.exports.getAllItems = getAllItems;
module.exports.getItemCategories = getItemCategories;
module.exports.getItem = getItem;
module.exports.constructProductAssetUrl = constructProductAssetUrl;
module.exports.findProductAssets = findProductAssets;
module.exports.getItemChoices = getItemChoices;
module.exports.addToWishlist = addToWishlist;
module.exports.getWishlistItems = getWishlistItems;
module.exports.getItemsByKeyword = getItemsByKeyword;
// functions used by RecommendedItems.js
module.exports.findProductAvailability = findProductAvailability;
module.exports.findProductDefinition = findProductDefinition;
module.exports.findProductCode = findProductCode;
module.exports.findProductPrice = findProductPrice;
module.exports.findProductBundles = findProductBundles;
