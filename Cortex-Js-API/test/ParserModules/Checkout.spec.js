const chai = require('chai');
const expect = chai.expect;
const testConfig = require('./../../Config/settings.js').settings.testConfig;
const Checkout = require('./../../ParserModules/Checkout.js');

const Utilities = require('./../../Utilities');
const Cortex = require('./../../Cortex.js');
const jsdom = require('jsdom').jsdom;
const doc = jsdom();
const window = doc.defaultView;
const $ = require('jquery')(window);

const TestInitializer = require('./ParserModulesTestInit.js');

describe('Checkout', function() {
  cortexInstance = null;
  before(function (done) {
    $.when(
      TestInitializer.instantiateTestCortexInstance(testConfig.isCognitoMode)
    ).done(
      (testCortexInstance) => {
        cortexInstance = testCortexInstance;
        done();
      }
    );
  });

  it(`checkout`, function(done) {
    $.when(Checkout.checkout(cortexInstance))
    .done( (cortexCheckoutData) => {
      console.log(cortexCheckoutData);
      done();
    })
    .fail((error)=>{
      console.log(error);
    });
  });

});
