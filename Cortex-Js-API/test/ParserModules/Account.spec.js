const chai = require('chai');
const expect = chai.expect;
const testConfig = require('./../../Config/settings.js').settings.testConfig;
const Accounts = require('./../../ParserModules/Accounts.js');

const Utilities = require('./../../Utilities');
const jsdom = require('jsdom').jsdom;
const doc = jsdom();
const window = doc.defaultView;
const $ = require('jquery')(window);

const TestInitializer = require('./ParserModulesTestInit.js');

describe('Accounts', function() {

    cortexInstance = null;
    before(function (done) {
      $.when(
        TestInitializer.instantiateTestCortexInstance(testConfig.isCognitoMode)
      ).done(
        (testCortexInstance) => {
          cortexInstance = testCortexInstance;
          done();
        }
      );
    });

    it(`getAllAccountInformation`, function(done) {
      $.when(Accounts.getAllAccountInformation(cortexInstance))
        .done((accountInformation) => {
          expect(accountInformation).to.not.be.null;
          done();
        });
    });

    it(`registerUser`, function(done) {
      this.timeout(5000);
      // this payload will just register the user
      const payload0 = {
        'firstName':'George',
        'lastName':'Harrison',
        'password':'password',
        'email':'george11.harrison@ep.com'
      };

      // this payload will register user with address and payment token
      const payload1 = {
        firstName:'George',
        lastName:'Harrison',
        password:'password',
        email:'george111.harrison@ep.com',
        address: {
          countryName: 'US',
          extendedAddress: '',
          locality: 'Richmond',
          postalCode: 'v7d923',
          region: 'VA',
          streetAddress: '8392 Fake Street',
          lastName: 'Harrison',
          firstName: 'George'
        },
        payment: {
          displayName: 'VISA',
          paymentToken: '1234dfsf'
        }
      };

      $.when(Accounts.registerUser(payload0, testConfig.instanceIP, testConfig.scope))
        .done((cortexInstance) => {
          expect(cortexInstance).to.not.be.null;
          done();
        })
        .fail((err) => {
          expect(err).to.not.be.null;
          done();
        });
    });

    it(`addUserAttributesToAccount`, function(done) {
      const payload = {
        address: {
          countryName: 'US',
          extendedAddress: '',
          locality: 'Richmond',
          postalCode: 'v7d923',
          region: 'VA',
          streetAddress: '8392 Fake Street',
          lastName: 'Bond',
          firstName: 'James'
        },
        payment: {
          displayName: 'VISA',
          paymentToken: '1234dfsf'
        }
      };

      $.when(Accounts.addUserAttributesToAccount(cortexInstance, payload))
        .done(() => {
          done();
        });
    });

    it(`getPaymentToken`, function(done) {
      $.when(Accounts.getPaymentToken(cortexInstance)).done((tokenData) => {
        console.log(tokenData);
        done();
      });
    });

    it(`getDefaultShippingAddress`, function(done) {
      $.when(Accounts.getDefaultShippingAddress(cortexInstance)).done((shippingAddressData)=>{
        console.log(shippingAddressData);
        done();
      });
    });

    it(`getDefaultBillingAddress`, function(done) {
      $.when(Accounts.getDefaultBillingAddress(cortexInstance)).done((shippingAddressData)=>{
        console.log(shippingAddressData);
        done();
      });
    });
});
