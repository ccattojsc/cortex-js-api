const jsdom = require('jsdom').jsdom;
const doc = jsdom();
const window = doc.defaultView;
const $ = require('jquery')(window);

const testConfig = require('./../../Config/settings.js').settings.testConfig;
const Utilities = require('./../../Utilities');

let Cognito = null;
try {
  Cognito = require('cortex-cognito').Cognito;
} catch (ex) {
    console.log('cortex-cognito unavailable');
}

function instantiateTestCortexInstance (isCognitoMode) {
  var dfd = $.Deferred();
  if (isCognitoMode) {
    cognitoInstance = new Cognito();
    $.when(Utilities.createCognitoCortexInstance(testConfig.EPShopperUsername, testConfig.EPShopperPassword, cognitoInstance)).done(
      (returnCortexInstance) => {
        dfd.resolve(returnCortexInstance);
      }
    );
  } else {
    $.when(Utilities.createCortexInstance(testConfig.EPShopperUsername, testConfig.EPShopperPassword, testConfig.instanceIP, testConfig.scope)).done(
      (returnCortexInstance) => {
        dfd.resolve(returnCortexInstance);
      }
    );
  }
  return dfd.promise();
}

module.exports.instantiateTestCortexInstance = instantiateTestCortexInstance;
