const chai = require('chai');
const expect = chai.expect;
const testConfig = require('./../../Config/settings.js').settings.testConfig;
const Orders = require('./../../ParserModules/Orders.js');

const Utilities = require('./../../Utilities');
const Cortex = require('./../../Cortex.js');

const TestInitializer = require('./ParserModulesTestInit.js');

const jsdom = require('jsdom').jsdom;
const doc = jsdom();
const window = doc.defaultView;
const $ = require('jquery')(window);

describe('Orders', function() {
  cortexInstance = null;
  before(function (done) {
    $.when(
      TestInitializer.instantiateTestCortexInstance(testConfig.isCognitoMode)
    ).done(
      (testCortexInstance) => {
        cortexInstance = testCortexInstance;
        done();
      }
    );
  });

  it(`getOrders`, function(done) {
    $.when(Orders.getOrders(cortexInstance)).done((orderData)=>{
      console.log(orderData);
      done();
    });
  });

  // TODO: finish all tests with the items.
});
