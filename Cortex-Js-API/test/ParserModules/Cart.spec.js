const chai = require('chai');
const expect = chai.expect;
const testConfig = require('./../../Config/settings.js').settings.testConfig;
const Cart = require('./../../ParserModules/Cart.js');

const Utilities = require('./../../Utilities');
const Cortex = require('./../../Cortex.js');
const jsdom = require('jsdom').jsdom;
const doc = jsdom();
const window = doc.defaultView;
const $ = require('jquery')(window);

const TestInitializer = require('./ParserModulesTestInit.js');

describe('Cart', function() {
  cortexInstance = null;
  before(function (done) {
    $.when(
      TestInitializer.instantiateTestCortexInstance(testConfig.isCognitoMode)
    ).done(
      (testCortexInstance) => {
        cortexInstance = testCortexInstance;
        done();
      }
    );
  });

  it(`shoppingCartParent`, function(done) {
    // TODO: shoppingCartParent.
    $.when(Cart.shoppingCartParent(cortexInstance)).done(
      (shoppingItemData)=>{
      console.log(shoppingItemData);
      done();
    });
  });

  it(`getShoppingCartQuantity`, function(done) {
    $.when(Cart.getShoppingCartQuantity(cortexInstance)).done( (quantity) => {
      console.log(quantity);
      done();
    });
  });

  it(`getShoppingCartTotal`, function(done) {
    $.when(Cart.getShoppingCartTotal(cortexInstance)).done((shoppingCartTotal)=>{
      console.log('getting the shopping cart total');
      console.log(shoppingCartTotal);
      done();
    });
  });

  it(`getPromotionDiscountForCart`, function(done) {
    // TODO: return if there is no promotion or discount for the current cart
    $.when(Cart.getPromotionDiscountForCart(cortexInstance)).done(
      (promotionDiscount) => {
        console.log(promotionDiscount);
        done();
      }
    );
  });

  it(`applyPromoCode`, function(done) {
    const promoCode = 'exampleCode';
    $.when(Cart.applyPromoCode(cortexInstance, promoCode))
    .done((promoCodeSuccess) => {
      console.log(promoCodeSuccess);
      done();
    })
    .fail((error) => {
      console.log(error);
    });
  });

  it(`isCartEmpty`, function(done) {
    $.when(Cart.isCartEmpty(cortexInstance)).done(
      (isCartEmpty) => {
        console.log(isCartEmpty);
      }
    );
  });

  it(`addToCart`, function(done) {
    const sku = 'VESTRI_DUAL_CHARGER_W_INSTALLATION';
    const quantity = 1;

    $.when(Cart.addToCart(cortexInstance, sku, quantity)).done(
      (addToCartReturnData) => {
        console.log(addToCartReturnData);
        done();
      }
    );
  });
});
