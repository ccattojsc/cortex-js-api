const chai = require('chai');
const expect = chai.expect;
const testConfig = require('./../../Config/settings.js').settings.testConfig;
const RecommendedProducts = require('./../../ParserModules/RecommendedItems.js');

const Utilities = require('./../../Utilities');
const jsdom = require('jsdom').jsdom;
const doc = jsdom();
const window = doc.defaultView;
const $ = require('jquery')(window);

const TestInitializer = require('./ParserModulesTestInit.js');

describe('RecommendedProducts', function() {
  cortexInstance = null;
  before(function (done) {
    $.when(
      TestInitializer.instantiateTestCortexInstance(testConfig.isCognitoMode)
    ).done(
      (testCortexInstance) => {
        cortexInstance = testCortexInstance;
        done();
      }
    );
  });

  it(`findCrosssellProducts`, function(done) {
    const sku = 'VESTRI_21_TURBINE_WHEEL_PACKAGE_GRAY';
    $.when(RecommendedProducts.findCrosssellProducts(cortexInstance, sku))
    .done((data) => {
      console.log(data);
      done();
    }).fail((err) => {
      console.log(err);
      done();
    });
  });

  it(`findRecommendedProducts`, function(done) {
    const sku = 'VESTRI_MENS_SS_HANLEY_CH_LG';
    $.when(RecommendedProducts.findRecommendedProducts(cortexInstance, sku)).done((recommendedProducts) => {
      console.log(recommendedProducts);
      done();
    })
    .fail((error) => {
      console.log(error);
    });
  });

});
