const chai = require('chai');
const expect = chai.expect;

// TODO: replace the configuration here.
// var testConfig = require('./../../Config/settings.js').settings.testConfig;
const testConfig = require('./../../Config/settings.js').settings.testConfig;

const Items = require('./../../ParserModules/Items.js');

const Utilities = require('./../../Utilities');
const Cortex = require('./../../Cortex.js');

const jsdom = require('jsdom').jsdom;
const doc = jsdom();
const window = doc.defaultView;
const $ = require('jquery')(window);

const TestInitializer = require('./ParserModulesTestInit.js');

describe('Items', function() {
  cortexInstance = null;
  before(function (done) {
    $.when(
      TestInitializer.instantiateTestCortexInstance(testConfig.isCognitoMode)
    ).done(
      (testCortexInstance) => {
        cortexInstance = testCortexInstance;
        done();
      }
    );
  });

  it(`getAllItems`, function(done) {
    this.timeout(10000);
    $.when(Items.getAllItems(cortexInstance, null)).done(
      (itemListData) => {
        console.log(itemListData);
        done();
      }
    );
  });

  it(`getItemCategories`, function(done) {
    $.when(Items.getItemCategories(cortexInstance, '/')).done(
      (categories) => {
        console.log(categories);
        done();
      }
    )
  });

  // TODO: finish all tests with the items.
  //
  it(`getItemChoices`, function(done) {
    const sku = '0408402OS';

    $.when(Items.getItemChoices(cortexInstance, sku)).done(
      (itemChoices) => {
        console.log(itemChoices);
        done();
      }
    );
  });

  it(`getItem`, function(done) {
    const sku = 'VESTRI_STAINLESS_STEEL_WATER_BOTTLE';
    console.log('get Item test code is running');
    $.when(Items.getItem(cortexInstance, sku)).done(
      (itemChoices) => {
        console.log(itemChoices);
        done();
      }
    );
  });

  it(`AddToWishlist`, function(done) {
    const sku = 'VESTRI_STAINLESS_STEEL_WATER_BOTTLE';
    $.when(Items.addToWishlist(cortexInstance, sku)).done(
      () => {
        done();
      }
    ).fail(
      (error) => {
        console.log(error);
      });
  });

  it(`GetWishlistItems`, function(done) {
    $.when(Items.getWishlistItems(cortexInstance)).done(
      (wishListItems) => {
        console.log('returning from the wishlist');
        console.log(wishListItems);
    });
  });

  it(`getItemsByKeyword`, function(done) {
    const keyword = 'vestri';
    $.when(Items.getItemsByKeyword(cortexInstance, keyword)).done(
      (keywordSearchResults) => {
        console.log('returning from the keyword search results');
        console.log(JSON.stringify(keywordSearchResults));
    });
  });
});
