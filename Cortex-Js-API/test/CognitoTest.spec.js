const chai = require('chai');
const expect = chai.expect; // we are using the "expect" style of Chai

const Cognito = require('cortex-cognito').Cognito;

const Cortex = require('./../Cortex.js');
const Cart = require('./../ParserModules/Cart.js');
const Utilities = require('./../Utilities.js');
const jwtDecode = require('jwt-decode');

const jsdom = require('jsdom').jsdom;
const doc = jsdom();
const window = doc.defaultView;
const $ = require('jquery')(window);

const testConfig = require('./../Config/settings.js').settings.testConfig;

let cognitoInstance;

describe('CognitoTests', () => {
  before(() => {
    cognitoInstance = new Cognito();
  });
  it('registration', (done) => {
    console.log('single user registration here');
    const email = 'guest@ep.com';
    const password = 'password';
    const phoneNumber = '+17782466669';
    const userId = 'guest@ep.com';
    const role = testConfig.role;
    const scope = testConfig.scope;
    const signupSuccessCallback = (err, result) => {
      if (err) {
        console.log(err);
        return;
      }
      const cognitoUser = result.user;
      console.log(`user name is ${cognitoUser.getUsername()}`);
      done();
    };

    cognitoInstance.createCognitoUser(
      email,
      password,
      phoneNumber,
      'guest user',
      userId,
      role,
      scope,
      signupSuccessCallback
    );
  });

  it('addUserToGroup', (done) => {
    console.log('addUserToGroup');
    const callback = (err, data) => {
      console.log('the callback');
      console.log(err, data);
      done();
    }
    cognitoInstance.addUserToGroup('Members', 'jerry.seinfield@ep.com', callback);
  });

  it('massRegistration', (done) => {
    var cognitoUsers = config.get('cognitoUsers');
    console.log(cognitoUsers);

    var size = Object.keys(cognitoUsers).length;
    console.log(size);

    for (const userNumber in cognitoUsers) {
      var userObj = cognitoUsers[userNumber];

      const signupSuccessCallback = (err, result) => {
        if (err) {
          console.log(err);
          return;
        }
        const cognitoUser = result.user;
        console.log(`user name is ${cognitoUser.getUsername()}`);

        // decrement and see if we should return the test.
        size -= 1

        if (size == 0) {
          done();
        }
      };

      cognitoInstance.createCognitoUser(
        userObj.email,
        userObj.password,
        userObj.phoneNumber,
        userObj.fullname,
        userObj.userId,
        userObj.role,
        userObj.scope,
        signupSuccessCallback,
      );
    }
  });

  it('accountConfirmation', () => {
    console.log('just before call confirm registration with phone');
    const email = 'alvin0324@hotmail.com';
    const confirmSuccessCallback = (err, result) => {
      if (err) {
        console.log(err);
        return;
      }
      console.log(JSON.toString(result));
    };
    const confirmationCode = '945391';
    Cognito.confirmRegistrationWithCode(email, confirmationCode, confirmSuccessCallback);
  });

  it('resend', () => {
    console.log('Resending the confirmation code');
    Cognito.resendConfirmationCode();
  });

  it('listUsers', (done) => {
    const listUsersCallback = (err, data) => {
      if (err) {
        // There was an error.
        console.log('There was an error querying users');
        console.log(err, err.stack);
      } else {
        console.log(data.Users[0]); // successful response
      }

      done();
    };
    cognitoInstance.listCognitoUsers(listUsersCallback);
  });

  it('listUsersInGroup', (done) => {
    console.log('listAllUsersInGroup is running');
    const listUsersCallback = (err, data) => {
      if (err) console.log(err, err.stack); // an error occurred
      else console.log(data);           // successful response
      done();
    };
    console.log(Cognito.listAllCognitoUsersInGroup);
    Cognito.listAllCognitoUsersInGroup(listUsersCallback, 'Administrators');
  });

  it('authenticate', () => {
    const username = 'alvin0324@hotmail.com';
    const password = 'password';
    const authenticateUserCallback = () => {
      console.log('Successfully authenticated with Cognito User Pool');
    };
    cognitoInstance.authenticateUser(
      username,
      password,
      authenticateUserCallback
    );
  });

  describe('CognitoCortexIntegrationTest', () => {
    // TODO: Could place a before function here to grab Cognito token first... but test would have to wait on some sort of promise here.
    it('getCortexResource', (done) => {
      const username = 'alvin0324@hotmail.com';
      const password = 'password';
      $.when(Utilities.createCognitoCortexInstance(username, password, cognitoInstance)).done(
        (cognitoCortexInstance) => {
          const successCallback = (quantityData) => {
            done();
          }
          Cart.getShoppingCartQuantity(cognitoCortexInstance, successCallback, (jqXHR) => {
            console.log(jqXHR);
          });
        }
      );
    });

    it('postCortexResource', (done) => {
      const username = testConfig.EPShopperUsername;
      const password = testConfig.EPShopperPassword;
      $.when(Utilities.createCognitoCortexInstance(username, password, cognitoInstance)).done(
        (cognitoCortexInstance) => {
          const successCallback = (addToCartData) => {
            console.log('cart Data returning');
            console.log(addToCartData);
            done();
          };
          cognitoCortexInstance.cortexAddToCart('0408402OS', '1', successCallback, (error) => {
            console.log(error);
          });
        }
      );

    });
  });
});
