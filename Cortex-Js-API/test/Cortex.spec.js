const chai = require('chai');
const expect = chai.expect;
const testConfig = require('./../Config/settings.js').settings.testConfig;

const Utilities = require('./../Utilities');
const Cortex = require('./../Cortex.js');
const jsdom = require('jsdom').jsdom;
const doc = jsdom();
const window = doc.defaultView;
const $ = require('jquery')(window);


describe('Cortex', function() {

  // Sets up the logged in cortex instance.
  cortexInstance = null;
  before(function (done) {
    $.when(Utilities.createCortexInstance(testConfig.EPShopperUsername, testConfig.EPShopperPassword, testConfig.instanceIP, testConfig.scope)).done(
      (returnCortexInstance) => {
        cortexInstance = returnCortexInstance;
        done();
      }
    );
  });

  // Sets up the anonymous cortex instance.
  anonCortexInstance = null;
  before(function (done) {
    $.when(Utilities.createAnonCortexInstance(testConfig.instanceIP, testConfig.scope)).done((instance)=>{
      anonCortexInstance = instance;
      done();
    })
  });

  it(`cortexRegister`, function(done) {
      const email = 'curtis.yarn@ep.com';
      const password = 'password';
      const firstName = 'Curtis';
      const lastName = 'Yarn';

      const errorOnRegister = (error) => {
        console.log(error);
      };

      $.when(anonCortexInstance.cortexRegister(email, password, firstName, lastName, errorOnRegister)).done((registrationData) => {
          console.log(registrationData);
      });

    });

    it(`cortexGetItem`, function(done) {
        const sku = '7854794_BLACK_XSM';
        $.when(cortexInstance.cortexGetItem(sku, 'price,code,recommendations')).done((itemData) => {
          console.log(itemData);
          done();
        });
    });

    it(`cortexAddToCart`, function(done) {
        const sku = 'VESTRI_DUAL_CHARGER_W_INSTALLATION';
        const quantity = 1;

        $.when(cortexInstance.cortexAddToCart(sku, quantity)).done((cortexSuccessMessage)=>{
          console.log(cortexSuccessMessage);
          done();
        });
    });

    it(`cortexDeleteFromCart`, function(done) {
        const sku = 'VESTRI_WOMENS_MODEL_X_TEE_WH_MD';

        $.when(cortexInstance.cortexDeleteFromCart(sku)).done((deleteData) => {
          console.log(deleteData);
          done();
        });

    });

    it(`cortexShowPaymentSelection`, function(done) {
        $.when(cortexInstance.cortexShowPaymentSelection()).done((paymentSelection) => {
          console.log(paymentSelection);
          done();
        });
    });

    it(`cortexCreateAddress`, function(done) {
        $.when(cortexInstance.cortexCreateAddress(
          'US',
          '',
          'Richmond',
          '1234',
          'VA',
          '1234 Fake Street',
          'Murray',
          'Bill'
        )).done((addressSuccessMessage) => {
          console.log(addressSuccessMessage);
          done();
        });
    });

    it('cortexCreatePaymentMethod', function(done) {
      $.when(cortexInstance.cortexCreatePaymentMethod(
        'VISA',
        '123423213'
      )).done((paymentMethodSuccessMessage) => {
        console.log(paymentMethodSuccessMessage);
        done();
      });
    });

    it('cortexFindInstanceIdFromSku', function(done) {
      const sku = '6032320_GRAY_MD';

      $.when(cortexInstance.cortexFindInstanceIdFromSku(
        sku
      )).done((instanceId) => {
        console.log('return from the instance id');
        console.log(instanceId);
        done();
      });

    });
});
