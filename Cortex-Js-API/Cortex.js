// ***************************************************************
// Copyright © 2018 Elastic Path Software Inc. All rights reserved.
// ***************************************************************
let $ = null;

try {
  // TODO: Only bring in JSDOM and NAJAX if the user says they are going to use the libs server side.
  const najax = require('najax');
  const jsdom = require('jsdom').jsdom;
  const doc = jsdom();
  const window = doc.defaultView;
  $ = require('jquery')(window);
  $.ajax = najax;
} catch (error) {
  $ = require('jquery');
}

// TODO: For readability we should create another class CognitoCortex that will have extra fields necessary to interact with Cognito and override certain functions.
// This other class will be in the Cognito node_module.
function Cortex(baseUrl, scope, token) {
  this.cortexBaseUrl = baseUrl;
  this.scope = scope;  // this scope is used for url links
  this.token = token;
}
// This should just be a static function that is exported.
Cortex.prototype.convertToObj = function (data) {
  if (typeof data === 'string') {
    if (data !== null && data.trim() !== '') {
      let returnData = null;
      try {
        returnData = JSON.parse(data);
      } catch (err) {
        console.log(err);
      }
      return returnData;
    }
  }
  return data;
};

/**
 * Sets the headers that will be sent with each request
 * @param  {[type]} mode [description]
 * @return {[type]}      [description]
 */
Cortex.prototype.setCortexHeaders = function (headerData) {
  var headers = null;

  if (headerData) {
    headers = {
      'Content-Type': 'application/json',
      Authorization: this.token,
      'X-Ep-User-Id': headerData.userId,
      'X-Ep-User-Roles': headerData.role,
      'X-Ep-User-Scopes': headerData.scope
    };
  }

  return headers;
}

Cortex.prototype.cortexGet = function (url, success, error) {
  $.ajax({
    url: url,
    type: 'GET',
    headers: { Authorization: 'bearer ' + this.token },
    timeout: 9000 // HAX because its so unstable...
  }).done(success).fail(error);
};

Cortex.prototype.cortexPost = function (url, data, success, error) {
  $.ajax({
    url: url,
    type: 'POST',
    headers: { Authorization: 'bearer ' + this.token },
    contentType: 'application/json',
    data: JSON.stringify(data)
  }).done(success).fail(error);
};

Cortex.prototype.cortexPostPromise = function (url, data) {
  var dfd = $.Deferred();
  $.ajax({
    url: url,
    type: 'POST',
    headers: this.headers ? this.headers : { Authorization: 'bearer ' + this.token },
    contentType: 'application/json',
    data: JSON.stringify(data)
  }).done(function (data) {
    dfd.resolve(data);
  }).fail(function (error) {
    console.log('error in cortex post promise');
    console.log(error);
    dfd.reject(error);
  });

  return dfd.promise();
};

Cortex.prototype.cortexPut = function (url, data) {
  var dfd = $.Deferred();
  $.ajax({
    url: url,
    type: 'PUT',
    headers: { Authorization: 'bearer ' + this.token },
    contentType: 'application/json',
    data: JSON.stringify(data)
  }).done(function (returnData) {
    dfd.resolve(returnData);
  }).fail(function (failData) {
    dfd.reject(failData);
  });
  return dfd.promise();
};

Cortex.prototype.cortexDelete = function (url, success, error) {
  $.ajax({
    url: url,
    type: 'DELETE',
    headers: { Authorization: 'bearer ' + this.token },
    contentType: 'application/json'
  }).done(success).error(error);
};

Cortex.prototype.cortexFindLink = function (data, rel) {
  for (var i = 0; i < data.links.length; i++) {
    var link = data.links[i];
    if (link.rel == rel) {
      return link;
    }
  }
};

Cortex.prototype.cortexLogin = function (email, password, success, error) {
  $.ajax({
    url: this.cortexBaseUrl + '/oauth2/tokens',
    type: 'post',
    data: {
      grant_type: 'password',
      scope: this.scope,
      role: 'REGISTERED',
      username: email,
      password: password
    }
  }).done(success).fail(error);
};

Cortex.prototype.cortexAnonLogin = function (success, error) {
  $.ajax({
    url: this.cortexBaseUrl + '/oauth2/tokens',
    type: 'post',
    data: {
      grant_type: 'password',
      scope: this.scope,
      role: 'PUBLIC',
      username: '',
      password: ''
    }
  }).done(success).error(error);
};

// TODO: Make Cognito login here.
Cortex.prototype.cortexCognitoLogin = function() {
  // Will login here and populate the proper variables to interface with cognito...
};

/**
 * Posts a user registration given user information
 * @param  {String} email           the new user email to be registered
 * @param  {String} password        the password of the new user
 * @param  {String} firstName       the first name of the user
 * @param  {String} lastName        the last name of the user
 * @param  {Function} errorOnRegister the error function if registration fails
 * @return {Object}                 returns a promise.  Resolved once server returns response
 *  NOTE:  This request will only work if this Cortex Instance is anonymously logged in.
 */
Cortex.prototype.cortexRegister = function (email, password, firstName, lastName, errorOnRegister) {
  var dfd = $.Deferred();

  var registerActionUrl = this.cortexBaseUrl + '/registrations/' + this.scope + '/newaccount/form?followlocation';

  this.cortexPost(registerActionUrl, {
    'given-name': firstName,
    'family-name': lastName,
    username: email,
    password: password
  }, function (data) {
    dfd.resolve(data);
  }, errorOnRegister);

  return dfd.promise();
};

/**
 * function gets item information
 * @param  {String} sku - the sku code of the item you would like to search
 * @param  {String} zoom - The attribute of the item you would like to zoom to.  EX.  can be 'price' or 'definition' or even 'recommendations:crosssell,price'
 * @return {JQuery.promise}
 */
Cortex.prototype.cortexGetItem = function (sku, zoom) {
  const getItemUrl = this.cortexBaseUrl + '/lookups/' + this.scope + '?zoom=itemlookupform';
  let dfd = $.Deferred();

  const success = (item) => {
    const itemObj = this.convertToObj(item);
    dfd.resolve(itemObj);
  };

  const error = (err) => {
    dfd.reject(err);
  }

  this.cortexGet(getItemUrl, (getItemData) => {
    var data = this.convertToObj(getItemData);
    var itemLookupActionUri = this.cortexFindLink(data._itemlookupform[0], 'itemlookupaction').href;
    var postUrl = void 0;

    if (zoom == null) {
      postUrl = itemLookupActionUri + '?followlocation';
    } else {
      postUrl = itemLookupActionUri + '?followlocation&zoom=' + zoom;
    }

    this.cortexPost(
      postUrl,
      {
        code: sku
      },
      success,
      error
    );
  }, error);

  return dfd.promise();
};

// FIXME - get rid of hardcoded elements in link and the zoom.
Cortex.prototype.cortexGetCategoryItems = function (categoryCode) {
  var dfd = $.Deferred();
  var url = this.cortexBaseUrl + '/navigations/' + this.scope + '/' + categoryCode + '=?zoom=items:element:code:item:definition';
  this.cortexGet(url, function (data) {
    dfd.resolve(data);
  }, function (error) {
    console.log('error trying to get the item');
    console.log(error);
  });
  return dfd.promise();
};

Cortex.prototype.cortexGetCategoryItemLinks = function (categoryCode) {
  var dfd = $.Deferred();
  var url = this.cortexBaseUrl + '/navigations/' + this.scope + '/' + categoryCode + '=?zoom=items';
  this.cortexGet(url, function (data) {
    dfd.resolve(data);
  }, function (error) {
    console.log('failing link');
    console.log(url);
    dfd.reject(error);
  });
  return dfd.promise();
};

Cortex.prototype.cortexGetCategoryItemLinks0 = function (href) {
  var _this2 = this;

  var dfd = $.Deferred();
  $.when(this.cortexGetZoomData(href + '?zoom=', 'items')).done(function (returnData) {
    var data = _this2.convertToObj(returnData);
    dfd.resolve(data);
  });
  return dfd.promise();
};

Cortex.prototype.cortexGetListOfCategories = function () {
  var dfd = $.Deferred();
  var urlSuffix = '/navigations/' + this.scope + '?zoom=element';
  var url = this.cortexBaseUrl + urlSuffix;
  this.cortexGet(url, function (data) {
    dfd.resolve(data);
  }, function (error) {
    console.log(error);
  });
  return dfd.promise();
};

Cortex.prototype.cortexGetNavigations = function () {
  var dfd = $.Deferred();
  var urlSuffix = '/navigations/' + this.scope;
  var url = this.cortexBaseUrl + urlSuffix;
  this.cortexGet(url, function (data) {
    dfd.resolve(data);
  }, function (error) {
    console.log(error);
  });
  return dfd.promise();
};

Cortex.prototype.cortexGetCategoryChildren = function (categoryCode, success) {
  var encodedCategoryCode = base32.encode(categoryCode);
  this.cortexGet(this.cortexBaseUrl + '/navigations/' + this.scope + '/' + encodedCategoryCode + '?zoom=child', success, function () {});
};

Cortex.prototype.cortexAddToCartHelper = function (itemCode, quantity) {
  const dfd = $.Deferred();

  const success = (cortexSuccessMessage) => {
    dfd.resolve(cortexSuccessMessage);
  }

  const error = (err) => {
    dfd.reject(err);
  }

  var addToCartUrl = this.cortexBaseUrl + '/carts/' + this.scope + '/default/lineitems/items/' + this.scope + '/' + itemCode + '=?followlocation';
  var dataPayload = { quantity: quantity };
  this.cortexPost(addToCartUrl, dataPayload, success, error);

  return dfd.promise();
};

/**
 * Adds the selected sku item to the signed in users cart
 * @param  {String} sku      - sku code of desired item to add to cart
 * @param  {Integer} quantity - the amount that should be added to cart
 * @return {[JQuery.promise]}
 */
Cortex.prototype.cortexAddToCart = function (sku, quantity) {
  const dfd = $.Deferred();

  const getInstanceId = function getInstanceId(uri) {
    let instanceIdTemp = uri.split('/');
    return instanceIdTemp[3].split('=')[0];
  };

  $.when(this.cortexGetItem(sku, null)).done((itemCallbackData) => {
    var data = this.convertToObj(itemCallbackData);
    var instanceId = getInstanceId(data.self.uri);
    $.when(this.cortexAddToCartHelper(instanceId, quantity))
    .done((cortexSuccessMessage) => {
        dfd.resolve(cortexSuccessMessage);
    })
    .fail((err) => {
        dfd.reject(err);
    });
  });

  return dfd.promise();
};

Cortex.prototype.cortexFindInstanceIdFromSku = function (sku) {
  var dfd = $.Deferred();

  var getInstanceId = function getInstanceId(uri) {
    var instanceIdTemp = uri.split('/');
    return instanceIdTemp[3].split('=')[0];
  };
  $.when(
    this.cortexGetItem(sku, null)
  ).done((itemCallbackData)=>{
    var data = this.convertToObj(itemCallbackData);
    var instanceId = getInstanceId(data.self.uri);
    dfd.resolve(instanceId);
  });

  return dfd.promise();
}

// TODO: Does not work with configurable items
/**
 * deletes the current item from cart.
 * @param  {String} sku - sku code of the item in the cart that would like to be deleted
 * @param  {Function} success   - the success callback if deletion is successfull
 * @param  {Function} error     - the error callback if deletion fails
 * @return null
 */
Cortex.prototype.cortexDeleteFromCart = function (sku) {
  const dfd = $.Deferred();

  var url = this.cortexBaseUrl + '/';
  $.when(this.cortexGetZoomData(url + '?zoom=', 'defaultcart:lineitems')).done((listOfLineItemsStr) => {
    var listOfLineItems = this.convertToObj(listOfLineItemsStr);
    var deleteLinks = listOfLineItems._defaultcart[0]._lineitems[0].links;
    deleteLinks.forEach( (deleteLink) => {
        if(deleteLink.rel === 'element') {
          $.when(this.cortexGetZoomData(deleteLink.href + '=?zoom=', 'item:code')).done( (lineItemsPidStr) => {
            var lineItemsPid = this.convertToObj(lineItemsPidStr);
            var code = lineItemsPid._item[0]._code[0].code;
            const success = (deleteData) => {
              const deleteDataObj = this.convertToObj(deleteData);
              dfd.resolve(deleteDataObj);
            }
            const error = (err) => {
              dfd.reject(err);
            }
            if (code === sku) {
              this.cortexDelete(deleteLink.href, success, error);
            }
          });
        }
    });
  });

  return dfd.promise();
};

Cortex.prototype.cortexGetProductIdFromLineItemHref = function (url) {
  var dfd = $.Deferred();
  var success = function success(data) {
    data.links.forEach(function (link) {
      if (link.rel === 'item') {
        var productIdTemp = link.uri.split('/');
        productIdTemp = productIdTemp[productIdTemp.length - 1];
        var productId = productIdTemp.substring(0, productIdTemp.length - 1);
        dfd.resolve(productId);
      }
    });
  };
  var error = function error(err) {
    console.log(err);
  };
  this.cortexGet(url, success, error);
  return dfd.promise();
};

// TODO: function uses the instance id instead of the sku code... may want to fix that here.
Cortex.prototype.cortexShowItemSelection = function (itemCode, success) {
  var suffixUrl = '/itemdefinitions/' + this.scope + '/' + itemCode + '=/options';
  var zoom = '?zoom=element:selector,element:selector';
  var url = this.cortexBaseUrl + suffixUrl + zoom;
  this.cortexGet(url, success, function (data) {
    console.log('unable to show items. ' + JSON.stringify(data));
  });
};

Cortex.prototype.cortexGetChoiceHrefs = function (url) {
  var dfd = $.Deferred();
  var extractHrefs = function extractHrefs(data) {
    data = this.convertToObj(data);
    var links = data.links;
    var returnHrefs = {
      description: '',
      selectAction: ''
    };
    links.forEach(function (obj) {
      if (obj.rel === 'description') {
        returnHrefs.description = obj.href;
      }
      if (obj.rel === 'selectaction') {
        returnHrefs.selectAction = obj.href;
      }
    });
    return dfd.resolve(returnHrefs);
  };

  this.cortexGet(url, extractHrefs.bind(this), function (error) {
    console.log(error);
  });

  return dfd.promise();
};

Cortex.prototype.cortexShowBillingAddresses = function () {
  var dfd = $.Deferred();
  var zoom = '/?zoom=defaultcart:order:billingaddressinfo:selector';
  var url = this.cortexBaseUrl + zoom;
  this.cortexGet(url, function (data) {
    dfd.resolve(data);
  }, function (data) {
    console.log('the error');
    console.log(data);
  });
  return dfd.promise();
};

/**
 * function will show the current payment option selected
 * @return {Object} - returns a jQuery promise.
 */
Cortex.prototype.cortexShowPaymentSelection = function () {
  var dfd = $.Deferred();
  var zoom = '/?zoom=defaultcart:order:paymentmethodinfo:selector';
  var url = this.cortexBaseUrl + zoom;
  this.cortexGet(url, function (data) {
    dfd.resolve(data);
  }, function (data) {
    console.log('the error');
    console.log(data);
  });
  return dfd.promise();
};

/**
 * Creates an address in the current account
 * @param  {String} countryCode     - Ex. CA, US, UK, EU
 * @param  {String} extendedAddress - Can be any string specifying more information
 * @param  {String} locality        - City
 * @param  {String} postalCode      - postalCode
 * @param  {String} region          - State or Province
 * @param  {String} streetAddress   - street address
 * @param  {String} lastName        - last name
 * @param  {String} firstName       - first name
 * @return null
 */
Cortex.prototype.cortexCreateAddress = function (countryCode, extendedAddress, locality, postalCode, region, streetAddress, lastName, firstName) {
  var dfd = $.Deferred();
  var url = this.cortexBaseUrl + '/addresses/' + this.scope + '?followlocation';

  var payload = {
    address: {
      'country-name': countryCode,
      'extended-address': extendedAddress,
      locality: locality,
      'postal-code': postalCode,
      region: region,
      'street-address': streetAddress
    },
    name: {
      'family-name': lastName,
      'given-name': firstName
    }
  };


  this.cortexPost(url, payload, function (data) {
    dfd.resolve(data);
  }, function (error) {
    console.log('error with the post');
    console.log(error);
  });

  return dfd.promise();
};

/**
 * Creates a new payment method selection for the user
 * @param  {String} displayName  - Display name of the payment method
 * @param  {String} paymentToken - tokenized payment information
 * @return null
 */
Cortex.prototype.cortexCreatePaymentMethod = function (displayName, paymentToken) {
  var dfd = $.Deferred();

  var addToCartUrl = this.cortexBaseUrl + '/paymenttokens/' + this.scope + '?followlocation';
  var dataPayload = {
    'display-name': displayName,
    token: paymentToken
  };
  this.cortexPost(addToCartUrl, dataPayload, function (data) {
    dfd.resolve(data);
  }, function (error) {
    console.log(error);
  });
  return dfd.promise();
};

Cortex.prototype.cortexGetPurchaseForm = function () {
  var dfd = $.Deferred();
  var zoom = '?zoom=defaultcart:order:purchaseform';
  var url = this.cortexBaseUrl + zoom;
  this.cortexGet(url, function (data) {
    dfd.resolve(data);
  }, function (err) {
    console.log(err);
    console.log('unable to get purchase form');
  });
  return dfd.promise();
};

Cortex.prototype.cortexCheckout = function () {
  var dfd = $.Deferred();
  $.when(this.cortexGetPurchaseForm()).done((purchaseFormData) => {
    var data = this.convertToObj(purchaseFormData);
    var purchaseFormURI = data._defaultcart[0]._order[0]._purchaseform[0].links[0].href + '?followlocation';
    this.cortexPost(purchaseFormURI, {}, // payload...
    function (checkoutData) {
      dfd.resolve(checkoutData);
    }, function (error) {
      dfd.reject(error);
    });
  });
  return dfd.promise();
};

// TODO: get rid of function.  generic url passed
Cortex.prototype.cortexGetPaymentDescription = function (url) {
  var dfd = $.Deferred();
  this.cortexGet(url, function (data) {
    dfd.resolve(data);
  }, function () {
    console.log('error retrieving billing address description');
  });
  return dfd.promise();
};

Cortex.prototype.cortexGetShoppingItems = function () {
  var _this6 = this;

  var dfd = $.Deferred();
  var url = this.cortexBaseUrl + '?zoom=defaultcart:lineitems:element';
  this.cortexGet(url, function (shoppingItemData) {
    var data = _this6.convertToObj(shoppingItemData);
    dfd.resolve(data);
  }, function (data) {
    console.log('cortexGetShoppingItems failed');
    console.log(data);
  });
  return dfd.promise();
};

// TODO: We dont need to pass a url.  We already have the url in the cortex instance.
Cortex.prototype.cortexGetZoomData = function (url, zoom) {
  var dfd = $.Deferred();
  var queryUrl = url + zoom;
  queryUrl = queryUrl.replace('==', '=');
  this.cortexGet(queryUrl, function (data) {
    dfd.resolve(data);
  }, function (error) {
    console.log('cortex zoom failed');
    dfd.reject(error);
  });
  return dfd.promise();
};

Cortex.prototype.cortexShoppingCartQuantity = function (success, failure) {
  var url = this.cortexBaseUrl + '/?zoom=defaultcart';
  this.cortexGet(url, success, failure);
};

Cortex.prototype.cortexShoppingCartLinePrice = function (success) {
  var url = this.cortexBaseUrl + '?zoom=defaultcart:total';
  this.cortexGet(url, success, function () {
    console.log('Unable to retrieve cart Price');
  });
};

Cortex.prototype.cortexCreateEmail = function (email, success, error) {
  var url = this.cortexBaseUrl + '/emails/' + this.scope + '?followlocation';
  // var url = this.cortexBaseUrl + '/emails/tesla?followlocation';
  // http://54.201.138.64:9080/cortex/emails/tesla?followlocation
  var emailPayload = {
    email: email
  };
  this.cortexPost(url, emailPayload, success, error);
};

Cortex.prototype.cortexGetOrders = function () {
  var _this7 = this;

  var dfd = $.Deferred();
  var orderZoom = 'defaultprofile:purchases:element';
  var zoomUrl = this.cortexBaseUrl + '?zoom=';
  $.when(this.cortexGetZoomData(zoomUrl, orderZoom)).done(function (zoomData) {
    var data = _this7.convertToObj(zoomData);
    dfd.resolve(data);
  });

  return dfd.promise();
};

Cortex.prototype.cortexGetPurchaseLineItems = function (href) {
  var _this8 = this;

  var dfd = $.Deferred();
  var zoom = 'element';
  var zoomUrl = href + '?zoom=';
  $.when(this.cortexGetZoomData(zoomUrl, zoom)).done(function (zoomData) {
    var data = _this8.convertToObj(zoomData);
    dfd.resolve(data);
  });
  return dfd.promise();
};

Cortex.prototype.cortexGetWishlist = function () {
  // TODO: This will get the initial zoom for the wishlist
  const dfd = $.Deferred();

  var url = this.cortexBaseUrl;
  $.when(this.cortexGetZoomData(url + '?zoom=', 'defaultwishlist:lineitems:element')).done(
    (returnData) => {
      dfd.resolve(returnData);
    }
  );

  return dfd.promise();
}

module.exports = Cortex;
